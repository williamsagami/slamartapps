// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true
};

export const firebaseConfig = {
  apiKey: "AIzaSyD3NxHVlLeo8K2NVDiPq7wq5WEhMtK52r4",
  authDomain: "slamart-big-projects.firebaseapp.com",
  projectId: "slamart-big-projects",
  storageBucket: "slamart-big-projects.appspot.com",
  messagingSenderId: "707880733056",
};
 
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
