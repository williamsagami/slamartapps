import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Home', url: '/home', icon: 'home' },
    { title: 'Alerts', url: '/alerts', icon: 'paper-plane' },
    { title: 'Guards', url: '/guards', icon: 'heart' },
    { title: 'Security', url: '/security', icon: 'archive' },
    { title: 'Account', url: '/account', icon: 'trash' },
    { title: 'Options', url: '/options', icon: 'warning' },
    { title: 'Map', url: '/security/map', icon: 'map' },
  ];
  constructor() {}
}
