import { Injectable } from '@angular/core';
import { ModalController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FirebaseService } from './firebase.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private firebaseService: FirebaseService, 
    private modalController: ModalController,
    private alertController: AlertController,
    private callNumber: CallNumber,
    private toastController: ToastController, 
    private iab : InAppBrowser,
    private loadingController: LoadingController) { }

  async showInfo(info: string, seconds: number) {
    const toast = await this.toastController.create({
      message: info,
      duration: seconds*1000,
      color: 'secondary',
    });
    toast.present();
  }

  sliderOptions(loop: boolean, delay: number) {
    let so = {
      loop: loop,
      autoplay: {
        delay: delay*1000,
      },
    }
    return so;
  }

  openLink(link: string) {
	  this.iab.create(link);
  }

  async callConfirm(phoneNumber: string) {
    const alert = await this.alertController.create({
      cssClass: 'cool-alert',
      header: "Llamar " + phoneNumber,
      buttons: [
        {
          text: "Cancelar",
          role: 'cancel',
          cssClass: 'secondary',
        }, 
        {
          text: "Llamar",
          handler: () => {
            this.callNumber.callNumber(phoneNumber, false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
          }
        }
      ]
    });

    await alert.present();
  }

  createLoading(message: string) {
    return new Promise<HTMLIonLoadingElement>((resolve, reject) => {
      this.loadingController.create({
        message: message,
        cssClass: 'cool-loading',
      }).then(loading => {
        loading.present().then(() => { 
          resolve(loading); 
        }).catch(err => reject(err));
      });
    });
  }

}
 