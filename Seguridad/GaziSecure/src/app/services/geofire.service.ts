import { Injectable, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import firebase from 'firebase/app';
import "firebase/database";
import { BehaviorSubject } from 'rxjs';
// import { 
//   BackgroundGeolocation,
//   BackgroundGeolocationConfig, 
//   BackgroundGeolocationEvents, 
//   BackgroundGeolocationResponse 
// } from '@ionic-native/background-geolocation/ngx';
// import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GeofireService implements OnInit{

  // GeoFire
  private geoFire: any;
  private geoFireReady = false;
  private radius = 50;
  private userLocation: BehaviorSubject<any[]> = new BehaviorSubject([0, 0]);
  private userLastLocation = [0, 0];
  private locationTimer: any;

  // GeoQuery References
  geoQuery: any;
  private geoQueryStarted = false;
  private geoQueryReady: any;
  private usersEntered: any;
  private usersMoved: any;
  private geolocationRef: any;

  // Firebase Data
  usersAround: BehaviorSubject<any[]> = new BehaviorSubject([]);

  constructor(
    // private backgroundGeolocation: BackgroundGeolocation,
    private geolocation: Geolocation,
    // private platform: Platform
    ) {}

  ngOnInit() {
    // this.platform.ready().then(() => {
    //   this.backgroundGeolocationSetup();
    // })

    // this.backgroundGeolocation.on("location" as any).subscribe(location => {
    //   console.log(location);
    // });
    // this.backgroundGeolocation.start();
  }
  
  getUserLocation() { return this.userLocation; }

  isGeofireReady() { return this.geoFireReady }

  geofireSetup(userId: string, isControlUser: boolean) {
    return new Promise<number[]>((resolve, reject) => {
      const geo = require("geofire");
      var dbRef = firebase.database().ref("gazisecure/locations");
      this.geoFire = new geo.GeoFire(dbRef);
      this.geoFireReady = true;
      console.log("GeoFire Setup");
  
      if(isControlUser) {
        this.setInitialLocation(userId).then(location => {
          resolve(location);
        });
      }
      else resolve(null);
    });
  }

  // backgroundGeolocationSetup() {
  //   const config: BackgroundGeolocationConfig = {
  //     desiredAccuracy: 0,
  //     stationaryRadius: 5,
  //     distanceFilter: 5,
  //     debug: true, //  enable this hear sounds for background-geolocation life-cycle.
  //     stopOnTerminate: false, // enable this to clear background location settings when the app terminates
  //   };

  //   this.backgroundGeolocation.configure(config).then(() => {
  //     this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
  //       this.backgroundGeolocation.startTask().then(task => {
  //         console.log("SI HACE ALGO")
  //         console.log(location);
  //         this.backgroundGeolocation.endTask(task);
  //       });

  //       // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
  //       // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
  //       // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
  //       //this.backgroundGeolocation.finish(); // FOR IOS ONLY
  //     });
  //   });

  //   this.backgroundGeolocation.start();
  // }

  setInitialLocation(userId: string) {
    return new Promise<number[]>((resolve, reject) => {
      if(this.geolocationRef) {
        resolve(this.userLocation.getValue());
      }
      else {
        this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(p => {
          const position = [p.coords.latitude, p.coords.longitude];
          this.userLocation.next(position);
          this.userLastLocation = position;
          this.geoFire.set(userId, position);
          console.log("Initial Location Set");
          resolve(position);
        }).catch(err => reject(err));
      }
    })
  }

  startLocationTimer(userId: string, seconds: number) {
    if(this.locationTimer || this.geolocationRef) {
      clearTimeout();
    }

    this.geolocationRef = this.geolocation.watchPosition({ enableHighAccuracy: true }).subscribe(p => {
      const position = p as any;
      this.userLocation.next([position.coords.latitude, position.coords.longitude]);
    });

    this.locationTimer = setInterval(() => {
      if(this.userLocation.getValue() !== this.userLastLocation) {
        this.userLastLocation = this.userLocation.getValue();
        this.geoFire.set(userId, this.userLocation.getValue());
        console.log("Location Set: ", this.userLocation.getValue());
      }
    }, seconds * 1000);

    console.log("Location Timer Started");
  }

  clearLocationTimer() {
    this.userLastLocation = this.userLocation.getValue();
    if(this.geolocationRef) this.geolocationRef.unsubscribe();
    if(this.locationTimer) clearTimeout(this.locationTimer);
    console.log("Location Timer Cleared");
  }

  startGeoQuery(userId: string) {
    return new Promise<void>((resolve, reject) => {
      if(!this.geoQueryStarted) {
        this.geoQuery = this.geoFire.query({
          center: this.userLocation.getValue(),
          radius: this.radius,
        });
    
        this.geoQueryReady = this.geoQuery.on("ready", () => {
          this.startLocationTimer(userId, 5);
          this.geoQueryStarted = true;
          console.log("GeoQuery Ready");
          resolve();
        });
    
        this.usersEntered = this.geoQuery.on("key_entered", (key, location, distance) => {
          // Fill the Array with Users Arround Data
          const user = {id: "id", distance: 999, location: {}};
          user.id = key;
          user.distance = distance;
          user.location = location;
    
          const usersAround = this.usersAround.getValue();
          let userExist = usersAround.find(u => u.id === key);
          if(userExist) {
            let userIndex = usersAround.findIndex(u => u.id === key);
            usersAround[userIndex] = user;
            this.usersAround.next(usersAround);
          }
          else {
            usersAround.push(user);
            this.usersAround.next(usersAround);
          }
        });
    
        this.usersMoved = this.geoQuery.on("key_moved", (key, location, distance) => {
          // Update the Array with Users Arround Data
          const usersAround = this.usersAround.getValue();
          let userExist = usersAround.find(u => u.id === key);
          if(userExist) {
            let userIndex = usersAround.findIndex(u => u.id === key);
            usersAround[userIndex].location = location;
            usersAround[userIndex].distance = distance;
            this.usersAround.next(usersAround);
          }
        });
      }
      else {
        console.log("GeoQuery Ready Again");
        this.clearLocationTimer();
        this.startLocationTimer(userId, 5);
        resolve();
      }
    });
  }

  getUserAround() {
    return this.usersAround;
  }

  getDistanceBeetwenPoints(point1: any[], point2: any[]) {
    const earthRadius = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(point2[0]-point1[0]);  // deg2rad below
    var dLon = this.deg2rad(point2[1]-point1[1]); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(point1[0])) * Math.cos(this.deg2rad(point2[0])) * 
      Math.sin(dLon/2) * Math.sin(dLon/2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    const distance = earthRadius * c; // Distance in km
    return distance;
  }
  
  deg2rad(degres) {
    return degres * (Math.PI/180);
  }
}
