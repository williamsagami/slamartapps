import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Platform } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { GeofireService } from './geofire.service';
import { BehaviorSubject } from 'rxjs';

export interface alert {
  id: string;
  creationDate: Date;
  closedDate: Date;
  location: number[];
  state: number;
  level: number; 
  user: user;
  lastChange: user;
  usersAround: any[];
}

export interface user {
  id: string,
  username: string,
  phone: string,
  email: string,
  images: string[],
  roles: string[],
  creationDate: Date,
  language: number,
  alertActive: string,
  alertCount: number,
  alertCancellations: number,
  alertAssigned: string,
  location: number[],
  titles: string[],
  description: string,
  token: string,
}

export interface post {
  id: string,
  creationDate: Date,
  title: string,
  content: string,
  images: string[],
  published: boolean,
  activateLink: boolean,
  linkType: string,
  linkedObject: any,
}

export enum state {
  inProgress,
  closed,
  cancelled,
}

export enum language {
  ES,
  EN
} 

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private dbName = "gazisecure";
  private dbRoute = "security/gazisecure/app"
  private userReady = false;
  private userAuthState: any;
  public userData: BehaviorSubject<user> = new BehaviorSubject(null);
  private appConfiguration: BehaviorSubject<any> = new BehaviorSubject(null);
  private appConfigReady = false;
  public defaultLanguage = language.EN;

  //User Setup
  private authRef: any;
  private userRef: any;
  private appConfigurationRef: any;

  constructor(
    public fbAuth: AngularFireAuth, 
    private db: AngularFirestore,
    private storage: AngularFireStorage,
    public fcm: FirebaseX,
    private geofireService: GeofireService,) {
      this.getAppData();
  }

  getDataBaseName() { return this.dbName; }
  getDatabaseRoute() { return this.dbRoute; }
  getUserId() { return this.userData.getValue().id; }
  getUserData() { return this.userData; }
  getAppConfiguration() { return this.appConfiguration }
  isUserReady() { return this.userReady; }
  isControlUser() { return (this.userHasRoles(["guard", "admin"])); }
  isAppConfigurationReady() { return this.appConfigReady; }

  //SETUP APP --------------------------------------------------------------------
  userSetup() { //Devuelve true si el usuario existe
    return new Promise((resolve, rejected) => {
      if(this.authRef) this.authRef.unsubscribe(); //Limpia las referencias de suscripcion
      this.authRef = this.fbAuth.authState.subscribe(user => { //Autenticacion de Usuario (Real Time)
        this.userAuthState = user;

        if(user) { 
          this.getUser(user.uid).then(userData => { 
            this.userData.next(userData as user);
            console.log("Auth Ready:", user.uid);

            // Account and User Data Exist
            if(userData) { 
              if(this.userRef) this.userRef.unsubscribe();
              this.userRef = this.getUserRealTime(user.uid).subscribe(userData => {
                this.userData.next(userData);
              });

              this.activateNotifications();
              this.userReady = true;
              console.log("User Ready");
              resolve(true);
            }
            // Account Exist but no User Data
            else {
              this.createUser(user.uid, user.email, "Guest").then(() => {
                this.logOut();
                console.log("User Created");
                resolve(false);
              });
            }
          }).catch(err => console.log(err));
        }
        // No Account
        else {
          this.userReady = true;
          console.log("User Ready");
          this.userData.next(null);
          resolve(false);
        }
      });
    });
  }

  getAppData() {
    // Get User Data
    this.userSetup().then(userExist => {

      // GeoFire Setup
      if(userExist && !this.geofireService.isGeofireReady()) {
        this.geofireService.geofireSetup(this.getUserId(), this.isControlUser()).then(location => {

          // If Control User get extra Setup
          if(this.isControlUser()) {
            const controlUser = this.userData.getValue();

            // Alert Assigned Remove
            if(this.userData.getValue().alertAssigned) {
              if(this.userData.getValue().alertAssigned !== "") controlUser.alertAssigned = "";
            }

            // Location Set
            if(location) controlUser.location = location;
            this.updateUser(controlUser);
          }
        });
      }
    });

    // APP CONFIGURATION
    this.db.collection(this.dbRoute).doc('appConfiguration').ref.get().then(appConfiguration => {
      this.appConfiguration.next(appConfiguration.data() as any);
      this.appConfigReady = true;
    });

    // if(this.appConfigurationRef) this.appConfigurationRef.unsubscribe();
    // this.appConfigurationRef = this.getAppConfigurationRealTime().subscribe(appConfiguration => {
    //   this.appConfiguration = appConfiguration;
    // });
  }

  userHasRoles(roles: string[], user?: user, hasAll?: boolean) {
    let rolesFound = [];
    if(user) {
      rolesFound = user.roles.filter(role => {
        if(roles.find(r => role === r)) return true;
        else return false;
      });

      if(hasAll) return rolesFound.length >= roles.length;
      else return rolesFound.length > 0;
    }
    else {
      if(this.userReady && this.userData.getValue()) {
        rolesFound = this.userData.getValue().roles.filter(role => {
          if(roles.find(r => role === r)) return true;
          else return false;
        });

        if(hasAll) return rolesFound.length >= roles.length;
        else return rolesFound.length > 0;
      }
      else return false;
    }
  }

  activateNotifications() {
    if(!this.userHasRoles(["admin"])) this.fcm.subscribe(this.dbName+"-sales");
    this.saveToken(this.getUserId());
  }

  //AUTENTICACION----------------------------------------------------------
  logIn(email: string, password: string) {
    return new Promise((resolve, rejected) => {
      this.fbAuth.signInWithEmailAndPassword(email, password).then(res => {
        this.getAppData();
        resolve(res);
      }).catch(err => rejected(err));
    });
  }

  logOut () {
    if(this.authRef) this.authRef.unsubscribe(); //Limpia las referencias de suscripcion
    if(this.userRef) this.userRef.unsubscribe();
    this.userReady = false;
    this.userAuthState = null;
    return this.fbAuth.signOut();
  }

  register(email: string, password: string, username?: string) {
    return new Promise((resolve, reject) => {
      this.fbAuth.createUserWithEmailAndPassword(email, password).then(res => {
        this.createUser(res.user.uid, email, username).then(() => {
        this.getAppData();
        resolve(res);
        }).catch(err => reject(err));
      }).catch(err => reject(err));
    });
  }

  sendPasswordResetEmail(email: string) {
    return this.fbAuth.sendPasswordResetEmail(email);
  }
  //GUARDAR TOKENS-------------------------------------------------------
  updateToken(userId: string, token: string) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(userId).update({token: token});
  }

  saveToken(userId: string) {
    return this.fcm.getToken().then(token => {
      return this.updateToken(userId, token).then(() => {
        return this.fcm.grantPermission();
      });
    });
  }

  //OPCIONES DE APLICACION-----------------------------------------
  updateAppConfiguration(appConfig: any) {
    return this.db.collection(this.dbRoute).doc('appConfiguration').update(appConfig);
  }

  // getAppConfiguration() {
  //   return new Promise((resolve, reject) => {
  //     this.db.collection(this.dbRoute).doc('appConfiguration').ref.get().then(res => {
  //       resolve(res.data());
  //     }).catch(err => reject(err));
  //   });
  // }

  // getAppConfigurationRealTime() {
  //   return this.db.collection(this.dbRoute).doc('appConfiguration').snapshotChanges().pipe(map(res => {
  //     return res.payload.data() as any;
  //   }));
  // }

  //BLOG----------------------------------------------------------
  getPostRealTime(post_id: string) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post_id).snapshotChanges().pipe(map(post => {
      const postData = post.payload.data() as any;
      postData.id = post.payload.id;
      postData.creationDate = postData.creationDate.toDate();
      return postData as post;
    }));
  }

  // getPostDataRealTime() {
  //   return this.db.collection(this.dbRoute).doc('adminPosts').snapshotChanges().pipe(map(postData => {
  //     return postData.payload.data() as any;
  //   }));
  // }

  getPosts() {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/adminPosts/posts', ref => ref
      .orderBy('creationDate', 'desc')).ref.get().then(res => {
        const posts = res.docs.map(post => {
          const postData = post.data() as any;
          postData.id = post.id;
          postData.creationDate = postData.creationDate.toDate();
          return postData as post;
        });
        resolve(posts);
      }).catch(err => reject(err));
    });
  }

  getPostsRealTime() {
    return this.db.collection(this.dbRoute+'/adminPosts/posts', ref => ref
    .orderBy('creationDate', 'desc'))
    .snapshotChanges().pipe(map(posts => {
      return posts.map(post => {
        const postData = post.payload.doc.data() as any;
        postData.id = post.payload.doc.id;
        postData.creationDate = postData.creationDate.toDate();
        return postData as post;
      });
    }));
  }

  async createPost(post: post) {
    let p = await this.db.collection(this.dbRoute+'/adminPosts/posts').add(post);
    return p;
  }

  updatePost(post: post) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post.id).update(post);
  }

  deletePost(post: post) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post.id).delete();
  }

  updatePostImages(post_id: string, images: string[]) {
    return this.db.collection(this.dbRoute+'/adminPosts/posts').doc(post_id).update({images: images});
  }

  //IMAGENES---------------------------------------------------------------
  uploadImage(documentRoute: string, imageFile: any, singleImage?: boolean) {
    return new Promise((resolve, reject) => {
      if(imageFile) {
        //Branch + dbName + Document + Collection + ObjectId + ImageName
        //const randomName = 'orders-' + this.dbName + '-adminPosts-posts-' + post.id+'-' + Math.random().toString().split(".")[1];
        
        const randomName = Math.random().toString().split(".")[1];
        this.getImageDimensions(imageFile).then(imageDimensions_ => {
          const imageDimensions = imageDimensions_ as any;

          console.log(imageDimensions);
          const data = documentRoute.split("/");
          const metaData = {
            customMetadata: { 
              branch: 'security',
              dbName: this.dbName,
              document: data[2],
              collection: data[3],
              objectId: data[4],
              width: imageDimensions.width, 
              height: imageDimensions.height,
              imageType: singleImage ? "single" : "multiple",
            }
          };

          this.storage.upload('security/'+this.dbName+'/'+data[3]+'/'+data[4]+'/'+randomName, imageFile, metaData).then(() => {
            resolve(randomName);
          });
        });
      }
      else { resolve("Error Uploading"); }
    });
  }

  getImageDimensions(imageFile: any) {
    return new Promise((resolve, reject) => {
      if (imageFile) {
        let img = new Image();
        img.onload = () => {
          //console.log(img.width+"x"+img.height);
          const imageDimensions = {width: img.width.toString(), height: img.height.toString()}
          resolve(imageDimensions);
        };
  
        let reader = new FileReader();
        reader.onload = (event: any) => {
          img.src = event.target.result;
        }
        reader.readAsDataURL(imageFile);
      }
      else resolve(null);
    });
  }

  //USUARIOS----------------------------------------------------------
  getUser(user_id: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userData/users').doc(user_id).ref.get().then(res => {
        resolve(res.data());
      }).catch(err => reject(err));
    });
  }

  getUserRealTime(user_id: string) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user_id).snapshotChanges().pipe(map(user => {
      return user.payload.data() as any;
    }));
  }

  getUsersRealTime() {
    return this.db.collection(this.dbRoute+'/userData/users', ref => ref
    .orderBy("username")).snapshotChanges().pipe(map(users => {
      return users.map(user => {
        const userData = user.payload.doc.data() as any;
        return userData as user;
      });
    }));
  }

  getUsersByRole(role: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userData/users').ref.where("roles", "array-contains", role).get().then(res => {
        let users = res.docs.map(user => {
          return user.data() as user;
        });
        resolve(users);
      }).catch(err => reject(err));
    });
  }

  getUsersByRoleRealTime(role: string) {
    return this.db.collection(this.dbRoute+'/userData/users', ref => ref
    .where("roles", "array-contains", role)).snapshotChanges().pipe(map(users => {
      return users.map(user => {
        const userData = user.payload.doc.data() as any;
        return userData as user;
      });
    }));
  }

  updateUser(user: any) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).update(user);
  }

  updateUserImages(user: user, images: string[]) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).update({images: images});
  }

  updateUserRoles(user: user, roles: string[]) {
    return this.db.collection(this.dbRoute+'/userData/users').doc(user.id).update({roles: roles});
  }

  //ALERTS----------------------------------------------------------
  getAlert(alert_id: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userAlerts/alerts').doc(alert_id).ref.get().then(alert => {
        const alertData = alert.data() as any;
        alertData.id = alert.id;
        alertData.creationDate = alertData.creationDate.toDate();
        alertData.closedDate = alertData.closedDate.toDate();
        resolve(alertData);
      }).catch(err => reject(err));
    });
  }

  getAlertsRealTime() {
    return this.db.collection(this.dbRoute+'/userAlerts/alerts', ref => ref
    .orderBy('creationDate')).snapshotChanges().pipe(map(alerts => {
      return alerts.map(alert => {
        const alertData = alert.payload.doc.data() as any;
        alertData.id = alert.payload.doc.id;
        alertData.creationDate = alertData.creationDate.toDate();
        alertData.closedDate = alertData.closedDate.toDate();
        return alertData as alert;
      });
    }));
  }

  getUserAlerts(user_id: string) {
    return new Promise((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userAlerts/alerts', ref => ref
      .where("user.id", "==", user_id)).ref.get().then(res => {
        var alerts = res.docs.map(alert => {
          const alertData = alert.data() as any;
          alertData.creationDate = alertData.creationDate.toDate();
          alertData.closedDate = alertData.closedDate.toDate();
          return alertData as alert;
        });
        resolve(alerts);
      }).catch(err => reject(err));
    });
  }

  updateAlert(alert: alert) {
    alert.lastChange = this.userData.getValue();
    return this.db.collection(this.dbRoute+'/userAlerts/alerts').doc(alert.id).update(alert);
  }

  closeAlert(alert: alert, userId?: string) {
    if(userId) {
      return this.updateUser({id: userId, alertActive: ""}).then(() => {
        alert.state = state.closed;
        alert.closedDate = new Date();
        this.updateAlert(alert);
      });
    }
    else {
      const user = this.userData.getValue();
      user.alertActive = "";
      this.userData.next(user);
      return this.updateUser(user).then(() => {
        alert.state = state.closed;
        alert.closedDate = new Date();
        this.updateAlert(alert);
      });
    }
  }

  cancelAlert(alert: alert, userId?: string) {
    if(userId) {
      this.getUser(userId).then(u => {
        const user = u as user;
        user.alertActive = "";
        user.alertCancellations++;

        return this.updateUser(user).then(() => {
          alert.state = state.cancelled;
          alert.closedDate = new Date();
          this.updateAlert(alert);
        });
      });
    }
    else {
      const user = this.userData.getValue();
      user.alertActive = "";
      user.alertCancellations++;
      this.userData.next(user);
      return this.updateUser(user).then(() => {
        alert.state = state.cancelled;
        alert.closedDate = new Date();
        this.updateAlert(alert);
      });
    }

  }

  createAlert(alert: alert) {
    return new Promise<string>((resolve, reject) => {
      this.db.collection(this.dbRoute+'/userAlerts/alerts').add(alert)
      .then(alertRef => {
        this.db.collection(this.dbRoute+'/userData/users').doc(this.userData.getValue().id).update({
          alertCount: this.userData.getValue().alertCount+1,
          alertActive: alertRef.id,
        }).then(() => resolve(alertRef.id));
      }).catch(err => reject(err));
    })
  }

  deleteAlert(alert: alert) {
    return this.db.collection(this.dbRoute+'/userAlerts/alerts').doc(alert.id).delete();
  }

  newUser() {
    const user = {} as user;
    user.id = "";
    user.username = "Guest";
    user.phone = "";
    user.email = "";
    user.images = [];
    user.roles = ["user"];
    user.creationDate  = new Date();
    user.alertCount = 0;
    user.alertCancellations = 0;
    user.alertActive = "";
    user.alertAssigned = "";
    user.location = [0, 0];
    user.titles = [];
    user.description = "";
    user.language = this.defaultLanguage;
    user.token = "token";

    return user;
  }

  createUser(user_id: string, email: string, username?: string, phone?: string) {
    return new Promise((resolve, reject) => {
      const user = this.newUser();
      user.id = user_id;
      user.username = username ? username : user.username;
      user.phone = phone ? phone : user.phone;
      user.email = email;

      this.db.collection(this.dbRoute+'/userData/users').doc(user.id).set(user).then(() => {
        resolve(user);
      }).catch(() => reject("Error Creating User"));
    });
  }
}