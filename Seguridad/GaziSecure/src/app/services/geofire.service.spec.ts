import { TestBed } from '@angular/core/testing';

import { GeofireService } from './geofire.service';

describe('GeofireService', () => {
  let service: GeofireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GeofireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
