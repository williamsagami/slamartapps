import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, post, user } from 'src/app/services/firebase.service';
import { UpdatePostPage } from './update-post/update-post.page';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;

  viewReady = false;
  userReady = false;
  user: user;

  posts: post[] = [];
  postsLength = 10;
  allPostsLoaded = false;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private modalController: ModalController) { }

  ngOnInit() {
    this.firebaseService.getPostsRealTime().subscribe(posts => {
      this.posts = posts;
      this.viewReady = true;
    });

    this.firebaseService.getUserData().subscribe(userData => {
      this.user = userData;
      this.userReady = true;
    });
  }

  isViewReady() { return this.viewReady && this.userReady; }

  isAdmin() { return this.firebaseService.userHasRoles(["admin"]); }

  scrollToTop() { this.content.scrollToTop(400); }

  getPosts() { return this.isAdmin() ? this.posts : this.posts.filter(post => post.published); }

  isPostEmpty(post: post) { return post.images.length < 1 && post.title === "" && post.content === ""; }

  createPost() {
    const post = {} as post;
    post.id = "";
    post.title = "";
    post.content = "";
    post.creationDate = new Date();
    post.images = [];
    post.published = false;

    this.data.createLoading("Creating Post...").then(loading => {
      this.firebaseService.createPost(post).then(docRef => {
        post.id = docRef.id;
        this.openPost(post).then(() => {
          this.postsLength++;
          loading.dismiss();
        });
      });
    });
  }

  updatePost(post: post) {
    if(this.isAdmin()) {
      this.data.createLoading("Opening Post...").then(loading => {
        this.openPost(post).then(() => loading.dismiss());
      });
    }
  }

  openPost(post: post) {
    return new Promise<void>((resolve, reject) => {
      this.modalController.create({
        component: UpdatePostPage,
        componentProps: {
          post: post,
        }
      }).then(modal => {
        modal.present().then(() => resolve());
      }).catch(err => reject(err));
    });
  }

  getLoadedPosts() {
    return this.getPosts().slice(0, Math.max(1, this.postsLength));
  }

  loadData(event) {
    setTimeout(() => {
      //Add products length
      this.postsLength += 10;
      event.target.complete();
  
      if(this.postsLength > this.getPosts().length) {
        this.postsLength = this.getPosts().length;
        event.target.disabled = true;
        this.allPostsLoaded = true;
        console.log("All Posts Loaded");
      }
    }, 500);
  }

}
