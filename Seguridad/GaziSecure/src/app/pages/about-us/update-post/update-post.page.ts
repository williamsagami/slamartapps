import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController, NavParams, PopoverController } from '@ionic/angular';
import { OptionsComponent } from 'src/app/components/options/options.component';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, post } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.page.html',
  styleUrls: ['./update-post.page.scss'],
})
export class UpdatePostPage implements OnInit {

  viewReady = false;
  post: post;
  postRef: any;
  documentRoute: string;
  loadingImage = true;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private modalController: ModalController,
    private navParams: NavParams,
    private popoverController: PopoverController) { 
      this.post = this.navParams.get("post");
    }

  ngOnInit() {
    this.gePost();
  }

  ionViewDidLeave() {
    if(this.postRef) this.postRef.unsubscribe();
  }

  closeWindow() { this.modalController.dismiss(); }

  allowPost() {
    return (this.post.title !== "" && this.post.content !== "") || this.post.images.length > 0;
  }

  gePost() {
    this.data.createLoading("Loading...").then(loading => {
      if(this.postRef) this.postRef.unsubscribe();
      this.postRef = this.firebaseService.getPostRealTime(this.post.id).subscribe(post => {
        this.post.images = post.images;
        this.documentRoute = "security/"+this.firebaseService.getDataBaseName()+"/adminPosts/posts/"+this.post.id;
        this.loadingImage = false;
        this.viewReady = true;
        loading.dismiss();
      });
    });
  }

  updatePost() {
    this.data.createLoading("Loading...").then(loading => {
      if(this.allowPost()) {
        this.post.published = true;
        this.firebaseService.updatePost(this.post).then(() => {
          this.data.showInfo("Post Updated", 1.5);
          loading.dismiss();
          this.closeWindow();
        }).catch(() => { loading.dismiss(); });
      } else {
        this.data.showInfo("The Post must habe at least 1 Image or Content to publish", 1.5);
        loading.dismiss();
      }
    });
  }

  async deletePostConfirm(ev: any) {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Delete Post",
        text: "Are you sure you want to Delete this Post?",
        options: [
          {text: "Delete", icon: "trash"},
          {text: "Cancel", icon: "close"}
        ],
      },
      event: ev,
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.deletePost(); } break;
      }
    });
  }

  deletePost() {
    if(this.postRef) this.postRef.unsubscribe();
    this.data.createLoading("Loading...").then(loading => {
      this.firebaseService.deletePost(this.post).then(() => {
        loading.dismiss();
        this.closeWindow();
        this.data.showInfo("Post Deleted", 1.5);
      }).catch(() => { loading.dismiss(); });
    });
  }

  // IMAGES ------------------------------------------------------------------------------------------------
  getPostImage() {
    if(this.post.images) {
      const image = this.post.images[0] ? this.post.images[0] : '../assets/images/image.png';
      if(image === "") return '../assets/images/image.png'; 
      else return image;
    }
    else return '../assets/images/image.png';
  }

  loadImage() {
    this.loadingImage = true;
  }

  async imageOptions() {
    const image = this.getPostImage();
    const isImageUploaded = this.post.images[0] ? true : false;
    if(isImageUploaded) {
      const popover = await this.popoverController.create({
        component: OptionsComponent,
        componentProps: {
          title: "Delete Image",
          text: "Are you sure you want to Delete the Post Image",
          options: [
            {text: "Delete", icon: "trash"},
            {text: "Cancel", icon: "close"},
          ],
        },
      });
      await popover.present();
  
      await popover.onWillDismiss().then(data => {
        console.log(data);
        const option = data.data;
        switch(option) {
          case 0: { this.deleteImage(image); } break;
        }
      });
    }
  }

  deleteImage(image: string) {
    let images = this.post.images as string[];
    images = images.filter(img => img !== image);

    this.firebaseService.updatePostImages(this.post.id, images);
  }



}
