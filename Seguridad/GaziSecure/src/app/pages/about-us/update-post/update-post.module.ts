import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdatePostPageRoutingModule } from './update-post-routing.module';

import { UpdatePostPage } from './update-post.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdatePostPageRoutingModule,
    ComponentsModule,
    LazyLoadImageModule,
  ],
  declarations: [UpdatePostPage]
})
export class UpdatePostPageModule {}
