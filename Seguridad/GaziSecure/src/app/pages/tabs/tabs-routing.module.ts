import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ControlGuard } from 'src/app/guards/control.guard';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { SubscriberGuard } from 'src/app/guards/subscriber.guard';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'contact',
        loadChildren: () => import('../contact/contact.module').then( m => m.ContactPageModule)
      },
      {
        path: 'account',
        loadChildren: () => import('../account/account.module').then( m => m.AccountPageModule),
        canActivate: [AuthGuard],
      },
      {
        path: 'home',
        loadChildren: () => import('../about-us/about-us.module').then( m => m.AboutUsPageModule)
      },
      {
        path: 'security',
        loadChildren: () => import('../security/security.module').then( m => m.SecurityPageModule),
        canActivate: [SubscriberGuard],
      },
      {
        path: 'alerts',
        loadChildren: () => import('../alerts/alerts.module').then( m => m.AlertsPageModule),
        canActivate: [ControlGuard],
      },
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
