import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,) { }

  ngOnInit() {
  }

  isControlUser() {
    return this.firebaseService.isControlUser();
  }

  getColor() {
    return this.firebaseService.userHasRoles(["admin", "guard", "subscriber"]) ? "" : "danger";
  }

  securityClick() {
    if(!this.firebaseService.userHasRoles(["admin", "guard", "subscriber"])) {
      this.data.showInfo("You need to be a Subscriber to use Security section services", 1.5);
    }
  }

}
