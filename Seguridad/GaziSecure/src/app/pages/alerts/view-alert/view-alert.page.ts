import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { OptionsComponent } from 'src/app/components/options/options.component';
import { DataService } from 'src/app/services/data.service';
import { alert, FirebaseService, user } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-view-alert',
  templateUrl: './view-alert.page.html',
  styleUrls: ['./view-alert.page.scss'],
})
export class ViewAlertPage implements OnInit {

  user: user;
  alert: alert;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private modalController: ModalController,
    private popoverController: PopoverController,
    private navParams: NavParams,
    private router: Router) { 
      this.alert = this.navParams.get("alert");
      this.firebaseService.getUserData().subscribe(userData => {
        this.user = userData;
      });
  }

  ngOnInit() {
    console.log(this.alert);
  }

  closeWindow() {
    this.modalController.dismiss();
  }

  isAdmin() { return this.firebaseService.userHasRoles(["admin"]); }

  assignAlert() {
    const guard = this.user;
    guard.alertAssigned = this.alert.user.id;
    this.firebaseService.updateUser(guard).then(() => {
      this.router.navigate(["/tabs/security/map"]);
      this.closeWindow();
      console.log("Alert Assigned");
    });
  }

  async closeAlertConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Close Alert",
        text: "Are you sure you want to Close the Alert?",
        options: [
          {text: "Close Alert", icon: "checkmark"},
          {text: "Cancel", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.closeAlert(); } break;
      }
    });
  }

  closeAlert() {
    this.data.createLoading("Closing Alert...").then(loading => {
      this.firebaseService.closeAlert(this.alert, this.alert.user.id).then(() => {
        console.log("Alert Closed");
        loading.dismiss();
        this.closeWindow();
      });
    });
  }

  async cancelAlertConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Cancel Alert",
        text: "Are you sure you want to Cancel the Alert?",
        options: [
          {text: "Cancel Alert", icon: "trash"},
          {text: "Close", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.cancelAlert(); } break;
      }
    });
  }

  cancelAlert() {
    this.data.createLoading("Cancelling Alert...").then(loading => {
      this.firebaseService.cancelAlert(this.alert, this.alert.user.id).then(() => {
        console.log("Alert Cancelled");
        loading.dismiss();
        this.closeWindow();
      });
    });
  }

}
