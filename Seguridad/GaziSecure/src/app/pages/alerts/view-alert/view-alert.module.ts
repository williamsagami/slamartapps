import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewAlertPageRoutingModule } from './view-alert-routing.module';

import { ViewAlertPage } from './view-alert.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewAlertPageRoutingModule,
    ComponentsModule,
  ],
  exports: [ViewAlertPage],
  declarations: [ViewAlertPage]
})
export class ViewAlertPageModule {}
