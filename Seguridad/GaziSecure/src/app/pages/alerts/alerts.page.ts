import { Component, OnInit, ViewChild  } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { alert, FirebaseService, state, user } from 'src/app/services/firebase.service';
import { GeofireService } from 'src/app/services/geofire.service';
import { ViewAlertPage } from './view-alert/view-alert.page';
import * as moment from 'moment';

export enum sort {
  old,
  new,
  distance,
}

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.page.html',
  styleUrls: ['./alerts.page.scss'],
})
export class AlertsPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;


  viewReady = false;
  user: user;
  searchbar = "";
  sorting = sort.new;
  filter = state.inProgress;

  alerts: alert[] = [];
  alertsRef: any;
  alertsLength = 10;
  allAlertsLoaded = false;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private modalController: ModalController,
    private geofireService: GeofireService) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService.getUserData().subscribe(userData => {
      this.user = userData;
    });

    this.alertsRef = this.firebaseService.getAlertsRealTime().subscribe(alerts => {
      this.alerts = alerts;
      this.viewReady = true;
    });
  }

  search(text: string) {
    this.searchbar = text;
  }

  getAlerts() {
    let alerts = this.alerts.filter(alert => alert.state === this.filter);

    switch(this.sorting) {
      case sort.new: {
        alerts = alerts.reverse();
        break;
      }
      case sort.distance:
        alerts = alerts.sort((a, b) => {
          if (this.geofireService.getDistanceBeetwenPoints(this.user.location, a.location) > this.geofireService.getDistanceBeetwenPoints(this.user.location, b.location)) return 1;
          else if (this.geofireService.getDistanceBeetwenPoints(this.user.location, a.location) < this.geofireService.getDistanceBeetwenPoints(this.user.location, b.location)) return -1;
          else return 0;
        });
    }
    return alerts;
  }

  searchAlerts() {
    if(this.searchbar === "") return this.getLoadedAlerts();
    else {
      let alerts = [] as alert[];
      this.getLoadedAlerts().forEach(alert => {
        if(alert.id.toLocaleLowerCase().startsWith(this.searchbar.toLocaleLowerCase()) || 
        alert.user.email.toLocaleLowerCase().startsWith(this.searchbar.toLocaleLowerCase())) alerts.push(alert);
      });
      return alerts;
    }
  }

  getUserImage(user: user) {
    return user.images[0] ? user.images[0] : './assets/images/user.png';
  }

  viewAlert(alert: alert) {
    this.data.createLoading("Loading...").then(loading => {
      this.modalController.create({
        component: ViewAlertPage,
        componentProps: {
          alert: alert,
        },
        swipeToClose: true,
      }).then(modal => {
        modal.present().then(() => loading.dismiss()).catch(err => console.log(err));
      });
    });
  }

  getAlertDistance(alert: alert) {
    return this.getDistanceText(this.geofireService.getDistanceBeetwenPoints(this.user.location, alert.location));
  }

  getDistanceText(distance: number) {
    if(distance < 1) return Number(distance * 1000).toFixed(2) + " m away";
    else return Number(distance).toFixed(2) + " km away";
  }

  getDateText(alert: alert) {
    return moment(alert.creationDate).fromNow();
  }

  getSortingText() {
    switch(this.sorting) {
      case sort.old:
        return "Date Old";
      case sort.new:
          return "Date New";
      case sort.distance:
        return "Distance";
    }
  }

  toggleSorting() {
    this.sorting++;
    if(this.sorting >= 3) this.sorting = 0;
  }

  getFilterText() {
    switch(this.filter) {
      case state.inProgress:
        return "In Progress";
      case state.closed:
          return "Closed";
      case state.cancelled:
        return "Cancelled";
    }
  }

  toggleFilter() {
    this.filter++;
    if(this.filter >= 3) this.filter = 0;
  }

  scrollToTop() { this.content.scrollToTop(400); }


  getLoadedAlerts() {
    return this.getAlerts().slice(0, Math.max(1, this.alertsLength));
  }

  loadData(event) {
    setTimeout(() => {
      //Add products length
      this.alertsLength += 10;
      event.target.complete();
  
      if(this.alertsLength > this.alerts.length) {
        this.alertsLength = this.alerts.length;
        event.target.disabled = true;
        this.allAlertsLoaded = true;
        console.log("All Alerts Loaded");
      }
    }, 500);
  }

}
