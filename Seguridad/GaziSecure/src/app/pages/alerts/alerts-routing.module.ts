import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ControlGuard } from 'src/app/guards/control.guard';

import { AlertsPage } from './alerts.page';

const routes: Routes = [
  {
    path: '',
    component: AlertsPage
  },
  {
    path: 'view-alert',
    loadChildren: () => import('./view-alert/view-alert.module').then( m => m.ViewAlertPageModule),
    canLoad: [ControlGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlertsPageRoutingModule {}
