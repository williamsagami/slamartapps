import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService, user } from 'src/app/services/firebase.service';
import { DataService } from 'src/app/services/data.service';
import { OptionsComponent } from 'src/app/components/options/options.component';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  viewReady = false;
  user: user;
  documentRoute: string;
  loadingProfilePic = true;

  title = "";

  constructor(
    private firebaseService: FirebaseService, 
    private popoverController: PopoverController,
    private router: Router, 
    private data: DataService) { }

  ngOnInit() {
    this.getAppData();
  }

  getAppData() {
    this.data.createLoading("Loading...").then(loading => {
      //Datos de Usuario
      this.firebaseService.getUserData().subscribe(userData => {
        if(userData) {
          this.user = userData;
          this.documentRoute = "security/"+this.firebaseService.getDataBaseName()+"/userData/users/"+this.user.id;
          this.loadingProfilePic = false;
          this.viewReady = true;
          loading.dismiss();
        }
      });
    });
  }

  logOut() {
    this.data.createLoading("Loging Out...").then(loading => {
      this.firebaseService.logOut().then(() => {
        loading.dismiss();
        this.router.navigate(['/login']);
      }).catch(() => { loading.dismiss(); });
    });
  }

  isAdmin() {
    return this.firebaseService.userHasRoles(["admin"]);
  }

  isControlUser() {
    return this.firebaseService.isControlUser();
  }

  getRoleText() {
    if(this.isAdmin()) return "Administrator";
    else if(this.firebaseService.userHasRoles(["guard"])) return "Guard";
    else if(this.firebaseService.userHasRoles(["subscriber"])) return "Subscriber";
    else return "User";
  }
  
  saveAccountChanges() {
    this.data.createLoading("Saving...").then(loading => {
      this.firebaseService.updateUser(this.user).then(() => {
        loading.dismiss();
        this.data.showInfo("Data saved correctly", 1.5);
      }).catch(() => { loading.dismiss(); });
    });
  }

  // IMAGES
  loadProfilePic() {
    this.loadingProfilePic = true;
  }

  getUserImage() {
    if(this.user.images) {
      const image = this.user.images[0] ? this.user.images[0] : '../assets/images/user.png';
      if(image === "") return '../assets/images/user.png'; 
      else return image;
    }
    else return '../assets/images/user.png';
  }

  async imageOptions() {
    const image = this.getUserImage();
    const isImageUploaded = this.user.images[0] ? true : false;
    if(isImageUploaded) {
      const popover = await this.popoverController.create({
        component: OptionsComponent,
        componentProps: {
          title: "Delete Image",
          text: "Are you sure you want to Delete the Post Image",
          options: [{text: "Delete", icon: "trash"}],
        },
      });
      await popover.present();
  
      await popover.onWillDismiss().then(data => {
        console.log(data);
        const option = data.data;
        switch(option) {
          case 0: { this.deleteImage(image); } break;
        }
      });
    }
  }

  deleteImage(image: string) {
    let images = this.user.images as string[];
    images = images.filter(img => img !== image);

    this.firebaseService.updateUserImages(this.user, images);
  }

  //GUARD OPTIONS
  addTitle() {
    if(this.title !== "") {
      if(this.user.titles) {
        this.user.titles.push(this.title);
        this.title = "";
      }
      else this.user.titles = [this.title];
    }
  }

  removeTitle(title: string) {
    if(this.user.titles) {
      this.user.titles = this.user.titles.filter(t => t !== title);
    }
  }
}
