import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.page.html',
  styleUrls: ['./options.page.scss'],
})
export class OptionsPage implements OnInit {

  viewReady = false;
  appConfiguration: any;

  phone = "";

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService.getAppConfiguration().subscribe(appConfiguration => {
      this.appConfiguration = appConfiguration;
      this.viewReady = true;
      console.log(this.appConfiguration);
    });
  }

  addPhone() {
    if(this.phone !== "") {
      if(this.appConfiguration.phones) {
        this.appConfiguration.phones.push({number: this.phone, text: this.phone});
        this.phone = "";
      }
      else this.appConfiguration.phones = [{number: this.phone, text: this.phone}];
    }
  }

  removePhone(phone: string) {
    if(this.appConfiguration.phones) {
      this.appConfiguration.phones = this.appConfiguration.phones.filter(p => p.number !== phone);
    }
  }

  updateAppConfiguration() {
    this.firebaseService.updateAppConfiguration(this.appConfiguration).then(() => {
      this.data.showInfo("App Configuration Saved", 1.5);
    });
  }

}
