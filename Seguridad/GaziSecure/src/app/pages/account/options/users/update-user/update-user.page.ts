import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, user } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.page.html',
  styleUrls: ['./update-user.page.scss'],
})
export class UpdateUserPage implements OnInit {

  viewReady = false;
  user: user;

  roles = [
    {
      text: "user",
      active: true,
    },
    {
      text: "admin",
      active: false,
    },
    {
      text: "guard",
      active: false,
    },
    {
      text: "subscriber",
      active: false,
    },
  ]

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private modalController: ModalController,
    private navParams: NavParams) {
    }

  ngOnInit() {
    this.user = this.navParams.get("user");
    if(this.user) {
      this.roles.forEach((role, i) => {
        if(this.firebaseService.userHasRoles([role.text], this.user)) this.roles[i].active = true;
      });
      this.viewReady = true;
    }
  }

  closeWindow() {
    this.modalController.dismiss();
  }

  updateUser() {
    this.data.createLoading("Updating User").then(loading => {
      let roles = [];
      this.roles.forEach(role => {
        if(role.active) roles.push(role.text);
      });
      this.firebaseService.updateUserRoles(this.user, roles).then(() => loading.dismiss()); 
    });
  }

}
