import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FirebaseService, user } from 'src/app/services/firebase.service';
import { UpdateUserPage } from './update-user/update-user.page';

enum sort {
  username,
  email,
  // date
}

enum filter {
  user,
  admin,
  guard,
  subscriber
}

@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  viewReady = false;
  users: user[] = [];
  searchbar = "";
  sorting = sort.username;
  filter = filter.user;

  constructor(
    private firebaseService: FirebaseService,
    private modalController: ModalController) { }

  ngOnInit() {
    this.firebaseService.getUsersRealTime().subscribe(users => {
      this.users = users;
      this.viewReady = true;
    });
  }

  getUserImage(user: user) {
    if(user.images) {
      const image = user.images[0] ? user.images[0] : '../assets/images/user.png';
      if(image === "") return '../assets/images/user.png'; 
      else return image;
    }
    else return '../assets/images/user.png';
  }

  async viewUser(user: user) {
    const modal = await this.modalController.create({
      component: UpdateUserPage,
      componentProps: {
        user: user,
      },
      swipeToClose: true,
    });
    modal.present();
  }

  search(text: string) {
    this.searchbar = text;
  }

  getUsers() {
    let users = this.users.filter(user => this.firebaseService.userHasRoles([this.getFilterText()], user));

    switch(this.sorting) {
      case sort.username: {
        users = users.sort((a, b) => {
          if (a.username > b.username) return 1;
          else if (a.username < b.username) return -1;
          else return 0;
        });
        break;
      }
      case sort.email: {
        users = users.sort((a, b) => {
          if (a.email > b.email) return 1;
          else if (a.email < b.email) return -1;
          else return 0;
        });
        break;
      }
      // case sort.date:
      //   users = users.sort((a, b) => {
      //     if (a.creationDate > b.creationDate) return 1;
      //     else if (a.creationDate < b.creationDate) return -1;
      //     else return 0;
      //   });
      //   break;
    }
    return users;
  }

  searchUsers() {
    if(this.searchbar === "") return this.getUsers();
    else {
      let users = [] as user[];
      this.getUsers().forEach(user => {
        if(user.username.toLocaleLowerCase().startsWith(this.searchbar.toLocaleLowerCase()) || 
        user.email.toLocaleLowerCase().startsWith(this.searchbar.toLocaleLowerCase())) users.push(user);
      });
      return users;
    }
  }

  getSortingText() {
    switch(this.sorting) {
      case sort.username:
        return "Username";
      case sort.email:
          return "Email";
      // case sort.date:
      //   return "Account Date";
    }
  }

  toggleSorting() {
    this.sorting++;
    if(this.sorting >= 2) this.sorting = 0;
  }

  getFilterText() {
    switch(this.filter) {
      case filter.user:
        return "user";
      case filter.admin:
          return "admin";
      case filter.guard:
        return "guard";
      case filter.subscriber:
        return "subscriber";
    }
  }

  toggleFilter() {
    this.filter++;
    if(this.filter >= 4) this.filter = 0;
  }
}
