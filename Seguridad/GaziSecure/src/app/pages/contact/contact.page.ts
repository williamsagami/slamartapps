import { Component, OnInit, ViewChild } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { PopoverController } from '@ionic/angular';
import { OptionsComponent } from 'src/app/components/options/options.component';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, user } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  user: user;
  appConfiguration: any;
  viewReady = false;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private callNumber: CallNumber,
    private popoverController: PopoverController,
    private launchNavigator: LaunchNavigator,
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.firebaseService.getUserData().subscribe(userData => {
      this.user = userData;
    });

    this.firebaseService.getAppConfiguration().subscribe(appConfiguration => {
      this.appConfiguration = appConfiguration;
      this.viewReady = true;
      console.log(this.appConfiguration);
    });
  }

  getAddress() {
    return this.appConfiguration ? this.appConfiguration.address : "ADDRESS EMPTY";
  }

  getDescription() {
    return this.appConfiguration ? this.appConfiguration.description : "DESCRIPTION EMPTY";
  }

  getSocialNetworks() {
    return this.appConfiguration ? this.appConfiguration.socialNetworks : [];
  }

  getAddressRoute() {
    return this.appConfiguration ? this.appConfiguration.addressRoute : "ADDRESS ROUTE EMPTY";
  }

  openLink(link: string) {
    this.data.openLink(link);
  }

  async callCustomerServiceConfirm() {
    let phones = [];
    this.appConfiguration.phones.forEach(phone => {
      phones.push({text: phone.number, icon: "call"});
    });
    phones.push({text: "Cancel", icon: "close"});

    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Call Customer Service",
        text: "Select a number to call:",
        options: phones,
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data ? data.data : 0;
      if(option < phones.length-1) {
        this.callNumber.callNumber(phones[option].text, true);
      }
    });
  }

  startRoute() {
    this.launchNavigator.isAppAvailable( this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable =>{
      var app;
      if(isAvailable){
          app =  this.launchNavigator.APP.GOOGLE_MAPS;
      }else{
          console.warn("Google Maps not available - falling back to user selection");
          app =  this.launchNavigator.APP.USER_SELECT;
      }

      this.launchNavigator.navigate(this.getAddressRoute(), {
        app: app
      });
    });
  }

}
