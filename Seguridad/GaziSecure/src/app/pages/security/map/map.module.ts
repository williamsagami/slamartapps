import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapPageRoutingModule } from './map-routing.module';

import { MapPage } from './map.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { AgmCoreModule } from '@agm/core';
import { LazyLoadImageModule } from 'ng-lazyload-image';
// import { AgmDirectionModule } from 'agm-direction';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapPageRoutingModule,
    ComponentsModule,
    LazyLoadImageModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyD3NxHVlLeo8K2NVDiPq7wq5WEhMtK52r4",
    }),
    // AgmDirectionModule,
  ],
  declarations: [MapPage]
})
export class MapPageModule {}
