import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { user } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-view-guard',
  templateUrl: './view-guard.page.html',
  styleUrls: ['./view-guard.page.scss'],
})
export class ViewGuardPage implements OnInit {

  viewReady = false;
  user: user;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams) {
    }

  ngOnInit() {
    this.user = this.navParams.get("user");
    if(this.user) {
      this.viewReady = true;
    }
  }

  closeWindow() {
    this.modalController.dismiss();
  }
}
