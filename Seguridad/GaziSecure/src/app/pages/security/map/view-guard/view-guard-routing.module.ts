import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewGuardPage } from './view-guard.page';

const routes: Routes = [
  {
    path: '',
    component: ViewGuardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewGuardPageRoutingModule {}
