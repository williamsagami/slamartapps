import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewGuardPageRoutingModule } from './view-guard-routing.module';

import { ViewGuardPage } from './view-guard.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewGuardPageRoutingModule,
    ComponentsModule,
  ],
  declarations: [ViewGuardPage]
})
export class ViewGuardPageModule {}
