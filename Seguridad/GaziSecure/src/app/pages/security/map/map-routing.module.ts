import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubscriberGuard } from 'src/app/guards/subscriber.guard';

import { MapPage } from './map.page';

const routes: Routes = [
  {
    path: '',
    component: MapPage
  },
  {
    path: 'view-guard',
    loadChildren: () => import('./view-guard/view-guard.module').then( m => m.ViewGuardPageModule),
    canActivate: [SubscriberGuard],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapPageRoutingModule {}
