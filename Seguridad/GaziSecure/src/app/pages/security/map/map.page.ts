import { Component, OnInit } from '@angular/core';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { FirebaseService, user } from 'src/app/services/firebase.service';
import { GeofireService } from 'src/app/services/geofire.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { ViewGuardPage } from './view-guard/view-guard.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  // Show Map
  mapReady = false;
  markersReady = false;
  // directionReady = false;
  bmActivate = 0;
  bmDeactivate = 0;

  // GeoFire
  center = [0, 0];
  userLocation = [0, 0];
  userLastLocation = [0, 0];

  // Firestore Data
  user = this.firebaseService.userData.getValue();
  usersAround = [];
  usersAroundRef: any;
  guards: any[];
  guardsRef: any;
  alertUser: any;

  // Direction
  // origin = {lat: 0, lng: 0}
  // destination = {lat: 1, lng: 1}
  // directionTimer: any;

  userIcon = {
    url: this.firebaseService.userHasRoles(["guard"]) ? './assets/images/guard-selected.svg' : './assets/images/user-selected.svg',
    scaledSize: {
      width: 30,
      height: 30
    },
  }

  guardIcon = {
    url:'./assets/images/guard.svg',
    scaledSize: {
      width: 30,
      height: 30
    },
  }

  guardOnWayIcon = {
    url: './assets/images/guard-assigned.svg',
    scaledSize: {
      width: 30,
      height: 30
    },
  }

  userAlertIcon = {
    url: './assets/images/user-alert.svg',
    scaledSize: {
      width: 30,
      height: 30
    },
  }

  guardAlertIcon = {
    url: './assets/images/guard-alert.svg',
    scaledSize: {
      width: 30,
      height: 30
    },
  }

  // directionRenderOptions = {
  //   suppressMarkers: true,
  //   polylineOptions: { strokeColor: '#fff', }
  // }

  public mapStyle = [
    {
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#1d2c4d"
        }
      ]
    },
    {
      "elementType": "labels.icon",
      "stylers": [
        {
          "color": "#3d91ff"
        }
      ]
    },
    {
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#61caff"
        }
      ]
    },
    {
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1b3d8d"
        }
      ]
    },
    {
      "featureType": "administrative.country",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#4b6878"
        }
      ]
    },
    {
      "featureType": "administrative.province",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#4b6878"
        }
      ]
    },
    {
      "featureType": "landscape.man_made",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#334e87"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#023e58"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#283d6a"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#6f9ba5"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1d2c4d"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#023e58"
        }
      ]
    },
    {
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#3C7680"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#304a7d"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#98a5be"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1d2c4d"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#2c6675"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [
        {
          "color": "#255763"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#b0d5ce"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#023e58"
        }
      ]
    },
    {
      "featureType": "road.highway.controlled_access",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#98a5be"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#1d2c4d"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#283d6a"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#3a4762"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [
        {
          "color": "#0e1626"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#4e6d70"
        }
      ]
    }
  ];

  constructor(
    private firebaseService: FirebaseService,
    private geofireService: GeofireService,
    private modalController: ModalController,
    private launchNavigator: LaunchNavigator,
    private backgroundMode: BackgroundMode,) {
      this.firebaseService.getUserData().subscribe(userData => {
        this.user = userData;
      });
    }

  ngOnInit() {}

  ionViewDidEnter() {
    this.geofireService.setInitialLocation(this.user.id).then(p => {
      this.center = p;
      this.userLocation = this.center;
      this.userLastLocation = this.center;
      this.mapReady = true;
      
      this.geofireService.startGeoQuery(this.user.id).then(() => {
        this.getData().then(() => {

          //Activate Background Mode
          // this.backgroundModeSetup();

          //Real Time Guard For Green Icon Update
          this.guardsRef = this.firebaseService.getUsersByRoleRealTime("guard").subscribe(guards => {
            this.guards = guards as user[];
          });

          this.usersAroundRef = this.geofireService.getUserAround().subscribe(usersAround => {
            this.usersAround = usersAround;
            this.markersReady = true;

            console.log("Someone Moved");

            //Update Assigned Alert User Location
            if(this.alertUser) {
              const u = this.usersAround.find(user => user.id === this.user.alertAssigned);
              this.alertUser.location = u.location;
              this.alertUser.distance = u.distance;
            }
          });
        });
        this.geofireService.getUserLocation().subscribe(userLocation => {
          this.userLocation = userLocation;
        });
      });
    });
  }

  ionViewDidLeave() {
    // this.backgroundMode.disable();
    this.geofireService.clearLocationTimer();
    if(this.usersAroundRef) this.usersAroundRef.unsubscribe();
    if(this.guardsRef) this.guardsRef.unsubscribe();
  }

  getData() {
    return new Promise<void>((resolve, reject) => {
      this.firebaseService.getUsersByRole("guard").then(guards => {
        this.guards = guards as any;
        if(this.alertAssigned()) {
          this.firebaseService.getUser(this.user.alertAssigned).then(user => {
            this.alertUser = user as any;
            resolve();
          });
        } 
        else resolve();
      }).catch(err => reject(err));
    });
  }

  onMapReady(map?: google.maps.Map ){
    if(map) {
      map.setOptions({
        streetViewControl: false,
        zoomControl: false,
        fullscreenControl: false,
      });
      map.setClickableIcons(false);
    }
  }

  backgroundModeSetup() {
    this.backgroundMode.enable();
    // this.backgroundMode.setDefaults({
    //   title: "Sharing Location",
    //   text: this.bmtest.toString(),
    //   color: "0a1428",
    //   resume: true,
    //   hidden: false,
    //   bigText: false
    // });

    this.backgroundMode.on('activate').toPromise().then(() => {
      // this.backgroundMode.disableWebViewOptimizations();
      // this.backgroundMode.disableBatteryOptimizations();
      this.bmActivate++;
      console.log("BMA")
    });
    this.backgroundMode.on('deactivate').toPromise().then(() => {
      this.bmDeactivate++;
      console.log("BMD")
    });
    this.backgroundMode.on('failure').toPromise().then(() => {
      console.log("ERROR BM");
    });
    // console.log("Background Mode: ", this.backgroundMode.isEnabled());
  }

  getDistanceText(distance: number) {
    if(distance < 1) return Number(distance * 1000).toFixed(2) + " m away";
    else return Number(distance).toFixed(2) + " km away";
  }

  getGuard(userId: string) {
    return this.guards.find(guard => guard.id === userId) as user;
  }

  isGuard(user: any) {
    return (this.getGuard(user.id) ? true : false) && user.id !== this.user.id;
  }

  getGuardIcon(guard: any) {
    if(guard.alertAssigned) {
      if(guard.alertAssigned === this.user.id) return this.guardOnWayIcon;
      else return this.guardIcon;
    }
    else return this.guardIcon;
  }

  getAlertIcon() {
    return this.firebaseService.userHasRoles(["guard"], this.alertUser) ? this.guardAlertIcon : this.userAlertIcon;
  }

  alertAssigned() { // Guard And Admin
    if(this.user.alertAssigned) {
      return this.firebaseService.isControlUser() && this.user.alertAssigned !== "";
    }
    else return false;
  }

  closestGuard() {
    let closestGuard = { distance: 100 };
    this.usersAround.forEach(user => {
      if(this.isGuard(user) && user.distance < closestGuard.distance) closestGuard = user;
    });
    return closestGuard;
  }

  startRoute(user: any) {
    this.launchNavigator.isAppAvailable( this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable =>{
      var app;
      if(isAvailable){
          app =  this.launchNavigator.APP.GOOGLE_MAPS;
      }else{
          console.warn("Google Maps not available - falling back to user selection");
          app =  this.launchNavigator.APP.USER_SELECT;
      }
      
      let cords = user.location;
      this.launchNavigator.navigate(cords[0]+", "+cords[1], {
        app: app
      });
    });
  }

  getUserImage(user: user) {
    if(user.images) {
      const image = user.images[0] ? user.images[0] : '../assets/images/user.png';
      if(image === "") return '../assets/images/user.png'; 
      else return image;
    }
    else return '../assets/images/user.png';
  }

  async viewUser(user: user) {
    const modal = await this.modalController.create({
      component: ViewGuardPage,
      componentProps: {
        user: user,
      },
      swipeToClose: true,
    });
    modal.present();
  }

  // startDirectionUpdate(seconds: number) {
  //   this.directionTimer = setInterval(() => {
  //     this.origin = this.getLocation();
  //     this.destination = this.getLocation(this.closestGuard());

  //     console.log(this.origin, this.destination);
  //     this.directionReady = true;
  //   }, seconds * 1000);
  // }

  // getLocation(user?: any) {
  //   if(user) {
  //     return {lat: user.location[0] as number, lng: user.location[1] as number};
  //   }
  //   else {
  //     return {lat: this.center[0] as number, lng: this.center[1] as number};
  //   }
  // }

}
