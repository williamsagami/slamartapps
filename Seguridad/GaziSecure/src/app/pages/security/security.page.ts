import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { OptionsComponent } from 'src/app/components/options/options.component';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService, alert, state, user } from 'src/app/services/firebase.service';
import { GeofireService } from 'src/app/services/geofire.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {

  viewReady = false;
  user: user;
  alert: alert;

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService,
    private popoverController: PopoverController,
    private geofireService: GeofireService) { 
      this.firebaseService.getUserData().subscribe(userData => {
        this.user = userData;
      });
    }

  ngOnInit() {
    this.getData();
  }

  getData() {
    if(this.alertActive()) {
      this.data.createLoading("Loading...").then(loading => {
        this.firebaseService.getAlert(this.user.alertActive).then(alert => {
          this.alert = alert as alert;
          this.viewReady = true;
          loading.dismiss();
        }).catch(err => console.log(err));
      });
    }
    else this.viewReady = true;
  }

  alertActive() { // User
    return this.user.alertActive !== "";
  }

  createAlert(level: number) {
    this.data.createLoading("Creating Alert...").then(loading => {
      this.geofireService.setInitialLocation(this.user.id).then(location => {
        const alert = {} as alert;
        alert.creationDate = new Date();
        alert.closedDate = new Date();
        alert.location = location;
        alert.state = state.inProgress;
        alert.level = level;
        alert.user = this.user;
        alert.lastChange = alert.user;
  
        this.geofireService.startGeoQuery(this.user.id).then(() => {
          const usersAround = this.geofireService.getUserAround().getValue();
          alert.usersAround = usersAround.filter(u => u.id !== alert.user.id);
          this.firebaseService.createAlert(alert).then(alertId => {
            this.alert = alert;
            this.alert.id = alertId;
            this.viewReady = true;
            console.log(alert);
            loading.dismiss();
          }).catch(err => console.log(err));
        });
      });
    });
  }

  async closeAlertConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Close Alert",
        text: "Are you sure you want to Close the Alert?",
        options: [
          {text: "Close Alert", icon: "checkmark"},
          {text: "Cancel", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.closeAlert(); } break;
      }
    });
  }

  closeAlert() {
    this.data.createLoading("Closing Alert...").then(loading => {
      this.firebaseService.closeAlert(this.alert).then(() => {
        this.geofireService.clearLocationTimer();
        console.log("Alert Closed");
        loading.dismiss();
      });
    });
  }

  async cancelAlertConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Cancel Alert",
        text: "Are you sure you want to Cancel the Alert?",
        options: [
          {text: "Cancel Alert", icon: "trash"},
          {text: "Close", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.cancelAlert(); } break;
      }
    });
  }

  cancelAlert() {
    this.data.createLoading("Cancelling Alert...").then(loading => {
      this.firebaseService.cancelAlert(this.alert).then(() => {
        this.geofireService.clearLocationTimer();
        console.log("Alert Cancelled");
        loading.dismiss();
      });
    });
  }

  async createAlertConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Meet Up",
        text: "Meet Up Confirmation?",
        options: [
          {text: "Confirm", icon: "checkmark"},
          {text: "Cancel", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.createAlert(1); } break;
      }
    });
  }

}
