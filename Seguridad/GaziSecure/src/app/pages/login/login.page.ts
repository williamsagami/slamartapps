import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  selectedTab = "login";
  username = "";
  email = "";
  password = "";
  rpassword = "";
  showPassword = false;

  constructor(
    private data: DataService,
    private firebaseService: FirebaseService, 
    private router: Router,) {
  }

  ngOnInit() {
  }

  togglePassword() {
    this.showPassword = !this.showPassword;
  }

  setTab(tab: string) {
    this.selectedTab = tab;
  }

  logIn() {
    this.data.createLoading("Loading...").then(loading => {
      this.firebaseService.logIn(this.email, this.password).then (res => {
        loading.dismiss();
        this.router.navigate(['/tabs/home']);
      }).catch(err => { 
        switch(err.code)
        {
          case 'auth/argument-error': { this.data.showInfo("Debes llenar todos los campos.", 1.5); } break;
          case 'auth/invalid-email': { this.data.showInfo("Email no valido.", 1.5); } break;
          case 'auth/user-not-found': { this.data.showInfo("Este email no esta registrado.", 1.5); } break;
          case 'auth/wrong-password': { this.data.showInfo("Contraseña incorrecta.", 1.5); } break;
        }
        
        this.password = null;
        loading.dismiss();
      });
    });
  }

  register() {
    this.data.createLoading("Loading...").then(loading => {
      var message = "";
      if(this.username === "") {message = "Debes llenar todos los campos."}
      if(this.username.length < 4) {message = "El nombre de usuario debe de tener al menos 4 caracteres."}

      if(message === "") {
        this.firebaseService.register(this.email, this.password, this.username).then (() => {
          loading.dismiss();
          this.router.navigate(['/tabs/home']);
        }).catch(err => { 
          console.log(err.code ? err.code : err);
          switch(err.code)
          {
            case 'auth/argument-error': { this.data.showInfo("Debes llenar todos los campos.", 1.5); } break;
            case 'auth/invalid-email': { this.data.showInfo("Email no valido.", 1.5); } break;
            case 'auth/weak-password': { this.data.showInfo("La contraseña debe tener al menos 6 caracteres.", 1.5); } break;
            case 'auth/email-already-in-use': { this.data.showInfo("Este email ya esta en uso.", 1.5); } break;
          }

          this.password = "";
          this.rpassword = "";
          loading.dismiss();
        });
      }
      else {
        this.data.showInfo(message, 1.5);
        this.password = "";
        this.rpassword = "";
        loading.dismiss();
      }
    });
  }

  sendResetPasswordEmail() {
    this.data.createLoading("Loading...").then(loading => {
      this.firebaseService.sendPasswordResetEmail(this.email).then(() => {
        this.data.showInfo("Se envio un correo para cambiar tu contraseña a esta direccion", 4);
        loading.dismiss();
      }).catch(error => {
        console.log(error);
        switch(error.code) {
          case 'auth/argument-error': { this.data.showInfo("Debes llenar todos los campos", 1.5); } break;
          case 'auth/invalid-email': { this.data.showInfo("Email no valido", 1.5); } break;
          case 'auth/user-not-found': { this.data.showInfo("Este email no esta registrado", 1.5); } break;
        }
        loading.dismiss();
      });
    });
  }
}

 