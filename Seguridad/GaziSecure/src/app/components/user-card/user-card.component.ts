import { Component, Input, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { PopoverController } from '@ionic/angular';
import { FirebaseService, user } from 'src/app/services/firebase.service';
import { AlertTimerComponent } from '../alert-timer/alert-timer.component';
import { OptionsComponent } from '../options/options.component';

@Component({
  selector: 'user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent implements OnInit {

  @Input() user: user;
  @Input() imageSize: string;
  @Input() hideEmail: boolean;
  @Input() hidePhone: boolean;
  @Input() callButton: boolean;
  @Input() callNotification: boolean;
  @Input() callTimer: number;

  constructor(
    private firebaseService: FirebaseService,
    private callNumber: CallNumber,
    private popoverController: PopoverController,) { }

  ngOnInit() {}

  getImageSize() {
    return this.imageSize ? this.imageSize : "50vw";
  }

  getUserImage() {
    if(this.user.images) {
      const image = this.user.images[0] ? this.user.images[0] : '../assets/images/user.png';
      if(image === "") return '../assets/images/user.png'; 
      else return image;
    }
    else return '../assets/images/user.png';
  }

  getRoleText() {
    if(this.firebaseService.userHasRoles(["admin"], this.user)) return "Administrator";
    else if(this.firebaseService.userHasRoles(["guard"], this.user)) return "Guard";
    else if(this.firebaseService.userHasRoles(["subscriber"], this.user)) return "Subscriber";
    else return "User";
  }
  
  hasTitles() {
    if(this.user.titles) {
      if(this.user.titles.length > 0) return true;
      else return false;
    }
    else return false;
  }

  hasDescription() {
    if(this.user.description) {
      if(this.user.description !== "") return true;
      else return false;
    }
    else return false;
  }

  async callUserConfirm() {
    const popover = await this.popoverController.create({
      component: OptionsComponent,
      componentProps: {
        title: "Call "+this.user.username,
        text: "Are you sure you want to Call "+this.user.username,
        options: [
          {text: "Call", icon: "call"},
          {text: "Close", icon: "close"},
        ],
      },
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      console.log(data);
      const option = data.data;
      switch(option) {
        case 0: { this.callUser(); } break;
      }
    });
  }

  callUser() {
    if(this.callNotification) {
      //CREATE DOCUMENT IN USER CALLS
      this.createCallTimer();
    }
    else {
      this.callNumber.callNumber(this.user.phone, true);
    }
  }

  async createCallTimer() {
    const popover = await this.popoverController.create({
      component: AlertTimerComponent,
      componentProps: {
        title: "Calling "+this.user.username,
        text: "Please Wait...",
        seconds: this.callTimer ? this.callTimer : 5,
      },
      backdropDismiss: false,
    });
    await popover.present();

    await popover.onWillDismiss().then(data => {
      this.callNumber.callNumber(this.user.phone, true);
    });
  }

}
