import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
})
export class SpinnerComponent implements OnInit {

  @Input() size: string;
  @Input() border: boolean;

  constructor() { }
  
  ngOnInit() {}
  
  getBorderWidth() {
    if(this.border) return "1px";
    else return "0px";
  }
}
