import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss'],
})
export class FileInputComponent implements OnInit {

  @Input() documentRoute: string;
  @Input() singleImage: boolean;
  @Output() imageUploaded = new EventEmitter() as any;

  selectedImages = [];

  constructor(
    private firebaseService: FirebaseService,
    private data: DataService) { }

  ngOnInit() {}

  // IMAGES ------------------------------------------------------------------------------------------------
  chooseImages(event) {
    this.selectedImages = event.target.files;

    let message = "";
    if(!this.selectedImages[0].type.startsWith("image")) message = "Invalid File";
    if(this.selectedImages[0].size/1024/1024 > 2) message = "The Image can't be bigger than 2mb";
    
    if(message !== "") {
      this.data.showInfo(message, 1.5);
    }
    else this.uploadPostImages();
    
    console.log(this.selectedImages[0].type);
  }

  async uploadPostImages() {
    for (var i = 0; i < this.selectedImages.length; i++) {
      this.emit();
    }

    for (var i = 0; i < this.selectedImages.length; i++) {
      var imageFile = this.selectedImages[i];

      let singleImage = false;
      if(this.singleImage) singleImage = true;
      await this.firebaseService.uploadImage(this.documentRoute, imageFile, singleImage).then(imgName =>{
        console.log(imgName);
      });
    }
  }

  emit() {
    this.imageUploaded.emit();
  }

}
