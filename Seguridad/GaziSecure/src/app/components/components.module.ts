import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OptionsComponent } from './options/options.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { AlertButtonComponent } from './alert-button/alert-button.component';
import { AlertCardComponent } from './alert-card/alert-card.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { FileInputComponent } from './file-input/file-input.component';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { UserCardComponent } from './user-card/user-card.component';
import { AlertTimerComponent } from './alert-timer/alert-timer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [
    SearchbarComponent,
    OptionsComponent,
    AlertButtonComponent,
    AlertCardComponent,
    SpinnerComponent,
    FileInputComponent,
    IconButtonComponent,
    UserCardComponent,
    AlertTimerComponent,
  ],
  exports:[
    SearchbarComponent,
    OptionsComponent,
    AlertButtonComponent,
    AlertCardComponent,
    SpinnerComponent,
    FileInputComponent,
    IconButtonComponent,
    UserCardComponent,
    AlertTimerComponent,
  ],
})
export class ComponentsModule {}