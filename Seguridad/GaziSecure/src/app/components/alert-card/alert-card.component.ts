import { Component, Input, OnInit } from '@angular/core';
import { alert, state, user } from 'src/app/services/firebase.service';
import * as moment from 'moment';

@Component({
  selector: 'alert-card',
  templateUrl: './alert-card.component.html',
  styleUrls: ['./alert-card.component.scss'],
})
export class AlertCardComponent implements OnInit {

  @Input() alert: alert;

  constructor() { }

  ngOnInit() {}

  getUserImage() {
    let image = '../assets/images/user.png';
    if(this.alert.user) {
      if(this.alert.user.images.length >= 1) image = this.alert.user.images[0];
    }
    return image;
  }

  getDateText(date: Date) {
    return moment(date).format('LLL');
  }

  isClosed() {
    return (this.alert.state === state.closed || this.alert.state === state.cancelled) ? true : false;
  }

  getStateText(alertState: number) {
    let stateText = "NONE";
    switch (alertState) {
      case state.inProgress: {
        stateText = "In Progress";
      } break;

      case state.closed: {
        stateText = "Closed";
      } break;

      case state.cancelled: {
        stateText = "Cancelled";
      } break;
    }
    return stateText;
  }

  getLevelText() {
    let levelText = "NONE";
    switch (this.alert.level) {
      case 1: {
        levelText = "Meet Up";
      } break;

      case 2: {
        levelText = "Alert";
      } break;
    }
    return levelText;
  }

}
