import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'alert-button',
  templateUrl: './alert-button.component.html',
  styleUrls: ['./alert-button.component.scss'],
})
export class AlertButtonComponent implements OnInit {

  @Input() progress: number;
  @Input() size: number;
  @Input() text: string;
  @Output() pressed = new EventEmitter() as any;

  radius: number;
  circumference: number;
  dashoffset: number;
  private timeoutHandler: any;
  textSize: number;
  button: any;

  constructor() {
  }

  ngOnInit() {
    this.radius = (this.size/2) - 6;
    this.circumference = 2 * Math.PI * this.radius;
    this.updateProgress(this.progress ? this.progress : 0);
    this.textSize = this.size/8;
  }

  ngAfterViewInit() {
    this.button = document.getElementById("alert-button");
  }

  private updateProgress(value: number) {
    const progress = value / 100;
    this.dashoffset = this.circumference * (1 - progress);
  }

  getButtonRadius() {
    return this.progress > 0 ? this.radius-8 : this.radius-4;
  }

  startCount(){
    if(this.timeoutHandler) this.endCount();

    this.progress = 0;
    this.timeoutHandler = setInterval(() => {
      if(this.progress >= 100) {
        this.doAction();
      }
      this.updateProgress(this.progress);
      this.progress += 0.33;
      this.progress = Math.min(this.progress, 100);
    }, 10);
  }

  endCount(){
    if (this.timeoutHandler) {
      clearTimeout(this.timeoutHandler);
      this.timeoutHandler = null;
    }
    this.progress = 0;
    this.updateProgress(this.progress);
  }

  isTouching(event) {
    let touch = event.touches[0];
    if (this.button !== document.elementFromPoint(touch.pageX,touch.pageY)) {
      this.endCount();
    }
  }

  doAction() {
    this.endCount();
    this.pressed.emit(true);
  }

}
