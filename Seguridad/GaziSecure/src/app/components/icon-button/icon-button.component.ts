import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
})
export class IconButtonComponent implements OnInit {

  @Input() name: string;
  @Input() color: string;
  @Input() iconColor: string;
  @Input() size: string;
  @Input() iconSize: string;
  @Input() disabled: boolean;
  @Output() pressed = new EventEmitter() as any;

  constructor() { }

  ngOnInit() {}

  getIconName() {
    return this.name ? this.name : "star";
  }

  getSize() {
    return this.size ? this.size : "48px";
  }

  getIconSize() {
    return this.iconSize ? this.iconSize : "32px";
  }

  getColor() {
    return this.color ? this.color : "secondary";
  }

  getIconColor() {
    return this.iconColor ? this.iconColor : "light";
  }

  emit() {
    this.pressed.emit();
  }

}
