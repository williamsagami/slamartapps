import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {

  currentOption = null;

  title = "";
  text = "";
  options = [
    {
      text: "Eliminar",
      icon: "trash"
    },
  ]

  constructor(private navParams: NavParams, private popoverController: PopoverController) { 
    this.title = this.navParams.get('title');
    this.text = this.navParams.get('text');
    this.options = this.navParams.get('options');
  }

  ngOnInit() {}

  closeView() {
    this.popoverController.dismiss(this.currentOption);
  }

  showText() { return this.title !== "" && this.text !== ""; }

  selectOption(value: number) {
    this.currentOption = value;
    this.closeView();
  }
}
