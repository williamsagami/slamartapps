import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-alert-timer',
  templateUrl: './alert-timer.component.html',
  styleUrls: ['./alert-timer.component.scss'],
})
export class AlertTimerComponent implements OnInit {

  title = "";
  text = "";
  seconds = 5;
  secondsText = 5;
  timer: any;

  constructor(private navParams: NavParams, private popoverController: PopoverController) { 
    this.title = this.navParams.get('title');
    this.text = this.navParams.get('text');
    this.seconds = this.navParams.get('seconds') ? this.navParams.get('seconds') : 5;
    this.secondsText = this.seconds;
  }

  ngOnInit() {
    this.timer = setInterval(() => {
      this.secondsText--;
      if(this.secondsText <= 0) this.closeView();
    }, this.seconds * 100);
  }

  closeView() {
    clearTimeout(this.timer);
    this.popoverController.dismiss();
  }

  showText() { return this.title !== "" && this.text !== ""; }

}
