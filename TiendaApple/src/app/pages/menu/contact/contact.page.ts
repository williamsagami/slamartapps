import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(
    private ds: DomSanitizer, 
    public dataService: DataService,
    private firebaseService: FirebaseService,
    private launchNavigator: LaunchNavigator) {
  }

  ngOnInit() {
  }

  getData() {
    return this.firebaseService.appConfiguration;
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.getData()?.mapSource);
  }

  openRoute() {
    this.launchNavigator.isAppAvailable( this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable =>{
      var app;
      if(isAvailable){
          app =  this.launchNavigator.APP.GOOGLE_MAPS;
      }else{
          console.warn("Google Maps not available - falling back to user selection");
          app =  this.launchNavigator.APP.USER_SELECT;
      }
      this.launchNavigator.navigate(this.getData()?.address, {
          app: app
      }).catch(() => {
        this.dataService.showInfo("Error al abrir la ruta", 1.5);
      });
    });
  }

}
 