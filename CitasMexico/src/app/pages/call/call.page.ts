import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ModalController, NavParams } from '@ionic/angular';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-call',
  templateUrl: './call.page.html',
  styleUrls: ['./call.page.scss'],
})
export class CallPage implements OnInit {

  t: any;

  phoneNumber: string;

  constructor(private modalController: ModalController, private navParams: NavParams, private callNumber: CallNumber,) { 
    this.phoneNumber = this.navParams.get('number');
    this.t = this.navParams.get('data');
  }

  ngOnInit() {
  }

  phoneCall() {
	  this.callNumber.callNumber(this.phoneNumber, false)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  
  closeCall(){
	  this.modalController.dismiss();
  }

}
