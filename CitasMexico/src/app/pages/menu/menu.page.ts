import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from '../../services/firebase.service';
import { ToastController, Platform, ModalController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  t: any;
  userReady = false;
  exit: any;
  user: any;

  selectedUrl = '/menu/inicio';

  pages = [
    {
      url: '/menu/inicio',
      icon: "home",
    },
    {
      url: '/menu/citas',
      icon: "list",
    },
    {
      url: '/menu/galeria',
      icon: "images",
    },
    {
      url: '/menu/contacto',
      icon: "person",
    }
  ]; 

  constructor(private router: Router, public data: DataService, 
    private firebaseService: FirebaseService, 
    private toastController: ToastController, 
    private platform: Platform, 
    private modalController: ModalController,
    private menuController: MenuController) { 
    this.t = data.appDataLocal.translate;
  }

  ionViewDidEnter(){
    /*this.router.events.subscribe((event: RouterEvent) => {
      this.selectedUrl = event.url ? event.url : this.selectedUrl;
    }); */

    this.exit = this.platform.backButton.subscribe(()=>{
      this.modalController.getTop().then(m => {
        if(!m) {
          this.menuController.isOpen().then(a => {
            if(!a) {
              if(this.selectedUrl === '/menu/inicio') navigator['app'].exitApp();
              else {
                this.router.navigate(['/menu/inicio']);
                this.selectedUrl = '/menu/inicio';
              }
            }
          });
        }
      });
    });
  }

  changeSelectedUrl(url: string) {
    this.selectedUrl = url;
  }

  ngOnInit() {
    this.firebaseService.userSetup().then(userDataExist => {
      this.userReady = userDataExist as any;
      this.data.setLanguage(this.firebaseService.userData.language);
    });
  }

  async showInfo(info: string) {
    const toast = await this.toastController.create({
      message: info,
      duration: 6000,
      color: 'secondary',
    });
    toast.present();
  }

  showSubscriptionButton() {
    if(this.userReady) {
      if(this.firebaseService.userData) {
        if(!this.firebaseService.userData.admin) return !this.firebaseService.userData.notifications;
        else return false;
      } 
    }
    else return false;
  }

  toggleSalesNotifications() {
    if(this.userReady) {
      if(!this.firebaseService.userData.notifications) {
        var userData = this.firebaseService.userData;
        userData.notifications = true;
        this.firebaseService.updateUser(userData).then(updateUser => { 
          this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName()+"-sales").then(showInfo => {
            this.showInfo(this.t.msg_NotificacionesActivadas[this.t.currentTranslation]);
          });
        });
      }
    }
    else {
      this.router.navigate(['/menu/login']);
    }
  }

}
