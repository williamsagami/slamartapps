import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

enum appointment_state {
    pending,
    accepted,
    rejected,
}

export enum language {
    ES,
    EN
}

admin.initializeApp(functions.config().firebase);

export const createAppointmentsDataBase = functions.https.onCall((data, context) => {
    return admin.firestore().collection('appointments').doc(data.dbName).set({}).then (() => {
        return admin.firestore().collection(data.dbRoute).doc('adminPosts').set({
            maxPostNumber: 8,
            postCount: 0,
            postNumber: 0,
        })
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('appConfiguration').set({
            dayRangeLimit: 10,
            maxUserRequestNumber: 2,
            maxUserRating: 10,
        });
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userData').set({});
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userRequests').set({});
    });
});

//APP DATA----------------------------------------------------------
export const updateAppData = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute).doc('appData').set(data.appData);
});

//NOTIFICATIONS-------------------------------------------------------------
export const updateToken = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute + '/userData/users').doc(data.userId).update({token: data.token});
});

export const onAppointmentWrite = functions.firestore.document('appointments/{dbName}/app/userRequests/requests/{requestId}')
.onWrite((request, context) => {
    
    const dbName = context.params.dbName;
    //const topic = dbName+"-admin";
    const userRequestBefore = request.before.exists ? request.before.data() : null;
    const userRequestAfter = request.after.exists ? request.after.data() : null;
    let result = null;

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: '',
            body: '',
        },
    };

    if(!request.before.exists) {
        //On Create
        const user = userRequestAfter ? userRequestAfter.user : {username: 'Guest', admin: false, language: language.EN};


        if(!user.admin) {
            //Get Admin List
            const admins: any[] = [];
            const adminTokens: string[] = [];
            return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
            .where("admin", "==", true).get().then(adminUsers => {
                adminUsers.docs.forEach(adminUser => {
                    if(adminUser.data()) {
                        admins.push(adminUser.data());
                        const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                        adminTokens.push(validToken);
                    }
                });

                //Send to Admin
                if(admins[0]) {
                    const adminUser = admins[0];
                    switch(adminUser.language) {
                        case language.EN: {
                            payload.notification.title = 'Appointment request pending.';
                            payload.notification.body = 'Created by ' + user.username;
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Solicitud de cita pendiente.';
                            payload.notification.body = 'Creada por ' + user.username;
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;
                    }
                }
            });
        }
    }
    else if(!request.after.exists) {
        //On Delete
        const user = userRequestBefore ? userRequestBefore.user : {username: 'Guest', admin: false, language: language.EN};
        const lastChange = userRequestBefore ? userRequestBefore.lastChange : {username: 'Guest', admin: false};
        const rsBefore = userRequestBefore ? userRequestBefore.state : appointment_state.pending;
        const rvBefore = userRequestBefore ? userRequestBefore.valid : true;

        //User Request Number Modification
        result = admin.firestore().collection('appointments/' + dbName + '/app/userData/users').doc(user.id).get().then(tempUser => {
            const tu = tempUser ? tempUser.data() : user;
            const rNumber = tu.requestNumber - 1;
            const lvl = rsBefore === appointment_state.accepted ? tu.level + 1 : tu.level;
            return admin.firestore().collection('appointments/' + dbName + '/app/userData/users').doc(tu.id).update({ requestNumber: rNumber, level: lvl });
        });

        if(!user.admin) {
            if(lastChange.admin && user.token && rvBefore) {
                //Send to User
                switch(user.language) {
                    case language.EN: {
                        payload.notification.title = 'An administrator has deleted your request.';
                        payload.notification.body = 'Your request did not meet what was required.';
                        result = admin.messaging().sendToDevice(user.token, payload);
                    } break;

                    case language.ES: {
                        payload.notification.title = 'Un administrador ha eliminado tu solicitud.';
                        payload.notification.body = 'Tu solicitud no cumplia con lo requerido.';
                        result = admin.messaging().sendToDevice(user.token, payload);
                    } break;
                }
            }
            else if(rvBefore) {
                //Get Admin List
                const admins: any[] = [];
                const adminTokens: string[] = [];
                return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
                .where("admin", "==", true).get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            adminTokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(rsBefore === appointment_state.accepted) {
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: {
                                    payload.notification.title = 'An appointment was canceled.';
                                    payload.notification.body = 'By ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Se cancelo una cita.';
                                    payload.notification.body = 'Cita de ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }
                });
            }
        }
    }
    else {
        //On Update
        const user = userRequestAfter ? userRequestAfter.user : {username: 'Guest', admin: false, language: language.EN};
        const lastChange = userRequestAfter ? userRequestAfter.lastChange : {username: 'Guest', admin: false};
        const rsBefore = userRequestBefore ? userRequestBefore.state : appointment_state.pending;
        const rsAfter = userRequestAfter ? userRequestAfter.state : appointment_state.pending;

        if(!user.admin) {
            if(lastChange.admin && user.token) {
                //Send to User
                if(rsBefore !== rsAfter && rsAfter === appointment_state.accepted) { //ACCEPT REQUEST
                    switch(user.language) {
                        case language.EN: {
                            payload.notification.title = 'Your request has been accepted.';
                            payload.notification.body = 'Open it for details.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Se ha aceptado tu solicitud.';
                            payload.notification.body = 'Revisa tu solicitud para los detalles.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
                else if(rsBefore !== rsAfter && rsAfter === appointment_state.rejected) //REJECT REQUEST
                {
                    switch(user.language) {
                        case language.EN: {
                            payload.notification.title = 'Your request has been rejected.';
                            payload.notification.body = 'Open it for details.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Tu solicitud ha sido rechazada.';
                            payload.notification.body = 'Revisa tu solicitud para los detalles.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
            }
            else {
                //Get Admin List
                const admins: any[] = [];
                const adminTokens: string[] = [];
                return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
                .where("admin", "==", true).get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            adminTokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(rsBefore !== appointment_state.pending && rsAfter === appointment_state.pending) { //FROM ACCEPTED/REJECTED TO PENDING BY USER
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: {
                                    payload.notification.title = 'A user modified an appointment.';
                                    payload.notification.body = 'By ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Un usuario modifico una cita.';
                                    payload.notification.body = 'Cita de ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }
                });
            }
        }
    }
    return result;
});

export const onAppointmentSaleWrite = functions.firestore.document('appointments/{dbName}/app/adminPosts/posts/{postId}')
.onCreate((post, context) => {
    const topic = context.params.dbName+"-sales";
    const adminPost = post.data();

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: adminPost.title,
            body: adminPost.content,
        },
    };

   return admin.messaging().sendToTopic(topic, payload);
});

//Apple Test
export const appleTest = functions.https.onRequest((request, response) => {

    const payload = {
        notification: {
            title: 'Salu2',
            body: 'Si sirve',
        },
    };

    response.send( 
        admin.firestore().collection('appleTest').doc('iphone').get().then(iphone => {
            const ip = iphone.data() ? iphone.data() as any : {token: ''};
            return admin.messaging().sendToDevice(
                ip.token, 
                payload
            );
        })
    );
});
