import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

enum alert_state {
    inProgress,
    closed,
    cancelled,
}

enum order_state {
    pending,
    accepted,
    cancelled,
    closed,
    inProgress,
}

enum language {
    ES,
    EN
}

admin.initializeApp(functions.config().firebase);

export const createAppointmentsDataBase = functions.https.onCall((data, context) => {
    return admin.firestore().collection('appointments').doc(data.dbName).set({}).then (() => {
        return admin.firestore().collection(data.dbRoute).doc('adminPosts').set({})
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('appConfiguration').set({});
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userData').set({});
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userRequests').set({});
    });
});

//APP DATA----------------------------------------------------------
export const updateAppData = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute).doc('appData').set(data.appData);
});

//NOTIFICATIONS-------------------------------------------------------------
export const updateToken = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute + '/userData/users').doc(data.userId).update({token: data.token});
});

// export const onAppointmentSaleWrite = functions.firestore.document('appointments/{dbName}/app/adminPosts/posts/{postId}')
// .onCreate((post, context) => {
//     const topic = context.params.dbName+"-sales";
//     const adminPost = post.data();

//     const payload = { //ESTRUCTURA DE NOTIFICACION
//         notification: {
//             title: adminPost.title,
//             body: adminPost.content,
//         },
//     };

//    return admin.messaging().sendToTopic(topic, payload);
// });

//Apple Test
export const appleTest = functions.https.onRequest((request, response) => {

    const payload = {
        notification: {
            title: 'Salu2',
            body: 'Si sirve',
        },
    };

    response.send( 
        admin.firestore().collection('appleTest').doc('iphone').get().then(iphone => {
            const ip = iphone.data() ? iphone.data() as any : {token: ''};
            return admin.messaging().sendToDevice(
                ip.token, 
                payload
            );
        })
    );
});

export const onImageUpload = functions.storage.object().onFinalize((object, context) => {
    const imgFullName = object.name ? object.name : "image";
    const contentType = object.contentType;
    
    const branch = object.metadata ? object.metadata.branch : "none";  //ESTOS DATOS SON NECESARIOS PARA PODER USAR SUBIDA DE IMAGENES EN CUALQUIER LUGAR
    const dbName = object.metadata ? object.metadata.dbName : "none"; 
    const document = object.metadata ? object.metadata.document : "none"; 
    const collection = object.metadata ? object.metadata.collection : "none"; 
    const objectId = object.metadata ? object.metadata.objectId : "none"; 
    const imageName = imgFullName.split('/')[imgFullName.split('/').length-1];
    const imageWidth = object.metadata ? parseInt(object.metadata.width) : 9999;
    const imageHeight = object.metadata ? parseInt(object.metadata.height) : 9999;

    if(((imageWidth < 1024 && imageHeight < 1024) || (imageName.includes('_1024x1024')) || contentType === "image/gif") 
    && branch !== 'none'
    && dbName !== 'none'
    && document !== 'none'
    && collection !== 'none'
    && objectId !== 'none'
    && imageName !== 'none') {
        const dbRoute = branch + '/' + dbName + '/app/' + document + '/' + collection;

        return admin.firestore().collection(dbRoute).doc(objectId).get().then(o => {
            const obj = o.data() ? o.data() as any : {};
            const images = obj.images as string[];

            let downloadUrl = object.mediaLink ? object.mediaLink.split("?")[0] : "none"; // 'https://www.googleapis.com/download/storage/v1/b/abcbucket.appspot.com/o/songs%2Fsong1.mp3.mp3'
            const secondPartUrl = object.mediaLink ? object.mediaLink.split("?")[1] : "none"; // 'generation=123445678912345&alt=media'

            if(downloadUrl !== "none") {
                downloadUrl = downloadUrl.replace("https://www.googleapis.com/download/storage", "https://firebasestorage.googleapis.com");
                downloadUrl = downloadUrl.replace("v1", "v0");

                downloadUrl += "?" + secondPartUrl.split("&")[1]; // 'alt=media'
                const token = object.metadata ? object.metadata.firebaseStorageDownloadTokens : "none"; 
                downloadUrl += "&token=" + token; 
                
                // Update Images
                images.push(downloadUrl); 
                return admin.firestore().collection(dbRoute).doc(objectId).update({ images: images }); 
            }
            else return null; 
        });
    }
    else return null; 
});

export const onAlertWrite = functions.firestore.document('orders/{dbName}/app/userOrders/orders/{alertId}')
.onWrite((alert, context) => {
    
    const dbName = context.params.dbName;
    const alertId = context.params.alertId;
    const userAlertBefore = alert.before.exists ? alert.before.data() : null;
    const userAlertAfter = alert.after.exists ? alert.after.data() : null;
    let result = null;

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: '',
            body: '',
        },
    };

    if(!alert.before.exists) { //On Create
        const user = userAlertAfter ? userAlertAfter.user : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};
        const alert = userAlertAfter ? userAlertAfter : {usersAround: []};
        const roleAdmin = (user.roles as string[]).find(role => role === "admin") ? true : false;

        if(!roleAdmin) {
            //Get Admin List
            const admins: any[] = [];
            const tokens: string[] = [];
            return admin.firestore().collection('security/' + dbName + '/app/userData/users')
            .where("roles", "array-contains", "admin").get().then(adminUsers => {
                adminUsers.docs.forEach(adminUser => {
                    if(adminUser.data()) {
                        admins.push(adminUser.data());
                        const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                        tokens.push(validToken);
                    }
                });

                //Get Guards Arround List
                const guards: any[] = [];
                return admin.firestore().collection('security/' + dbName + '/app/userData/users')
                .where("roles", "array-contains", "guard").get().then(guardUsers => {
                    guardUsers.docs.forEach(guardUser => {
                        const guard = alert.usersAround.find((g: any) => g.id === guardUser.data().id);
                        if(guard) {
                            guards.push(guardUser.data());
                            const validToken = (guardUser.data().token !== "" && guardUser.data().token !== null) ? guardUser.data().token : "TOKEN";
                            tokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(admins[0]) {
                        const adminUser = admins[0];
                        switch(adminUser.language) {
                            case language.EN: 
                            default: {
                                payload.notification.title = 'Alert In Progress';
                                payload.notification.body = 'Alert: '+ alertId +' created by the user: '+user.username+' ('+user.email+')';
                                result = admin.messaging().sendToDevice(tokens, payload);
                            } break;
        
                            case language.ES: {
                                payload.notification.title = 'Alerta En Progreso';
                                payload.notification.body = 'Alerta: '+ alertId +' creada por el usuario: ' +user.username+' ('+user.email+')';
                                result = admin.messaging().sendToDevice(tokens, payload);
                            } break;
                        }
                    }
                });
            });
        }
    }
    else if(alert.before.exists && alert.after.exists){ //ON UPDATE -------------------------------------------------------------
        
        // User that created the order and the last account that changed it
        const user = userAlertAfter ? userAlertAfter.user : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};
        const alert = userAlertAfter ? userAlertAfter : {usersAround: []};
        const lastChange = userAlertAfter ? userAlertAfter.lastChange : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};

        // Order States Before and After Update
        const osBefore = userAlertBefore ? userAlertBefore.state : order_state.pending;
        const osAfter = userAlertAfter ? userAlertAfter.state : order_state.pending;

        // Roles from User
        let roleAdmin = (user.roles as string[]).find(role => role === "admin") ? true : false;

        // Roles from Last Change Account
        let lcRoleAdmin = (lastChange.roles as string[]).find(role => role === "admin") ? true : false;

        if(!roleAdmin) {
            if(lcRoleAdmin && user.token !== "") {
                // Send to User
                if(osBefore !== osAfter && osAfter === alert_state.closed) { // CLOSE REQUEST
                    // console.log(user);
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Alert Closed';
                            payload.notification.body = 'Your Alert has been Closed by an Administrator';
                            result = admin.messaging().sendToDevice(user.token, payload);
                            console.log(result);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Alerta Completada';
                            payload.notification.body = 'Tu Alerta ha sido Completada por un Administrador';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
                else if(osBefore !== osAfter && osAfter === order_state.cancelled) // CANCEL REQUEST
                {
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Alert Cancelled';
                            payload.notification.body = 'Your Alert has been Cancelled by an Administrator';
                            result = admin.messaging().sendToDevice(user.token, payload);
                            console.log(result);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Alerta Cancelada';
                            payload.notification.body = 'Tu Alerta ha sido Cancelada por un Administrador';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
            }
            else if(!lcRoleAdmin){
                //Get Admin List
                const admins: any[] = [];
                const tokens: string[] = [];
                return admin.firestore().collection('security/' + dbName + '/app/userData/users')
                .where("roles", "array-contains", "admin").get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            tokens.push(validToken);
                        }
                    });

                    //Get Guards Arround List
                    const guards: any[] = [];
                    return admin.firestore().collection('security/' + dbName + '/app/userData/users')
                    .where("roles", "array-contains", "guard").get().then(guardUsers => {
                        guardUsers.docs.forEach(guardUser => {
                            const guard = alert.usersAround.find((g: any) => g.id === guardUser.data().id);
                            if(guard) {
                                guards.push(guardUser.data());
                                const validToken = (guardUser.data().token !== "" && guardUser.data().token !== null) ? guardUser.data().token : "TOKEN";
                                tokens.push(validToken);
                            }
                        });

                        //Send to Admin
                        if(osBefore === alert_state.inProgress && osAfter === alert_state.closed) { //FROM PROGRESS TO CLOSED BY USER
                            if(admins[0]) {
                                const adminUser = admins[0];
                                switch(adminUser.language) {
                                    case language.EN: 
                                    default: {
                                        payload.notification.title = 'Alert Closed';
                                        payload.notification.body = 'Alert: '+alertId+' has been Closed by the user: '+user.username+' ('+user.email+')';
                                        result = admin.messaging().sendToDevice(tokens, payload);
                                    } break;
        
                                    case language.ES: {
                                        payload.notification.title = 'Alerta Completada';
                                        payload.notification.body = 'Alerta: '+alertId+' fue Completada por el usuario: '+user.username+' ('+user.email+')';
                                        result = admin.messaging().sendToDevice(tokens, payload);
                                    } break;
                                }
                            }
                        }

                        if(osBefore === alert_state.inProgress && osAfter === alert_state.cancelled) { //FROM PROGRESS TO CANCELLED BY USER
                            if(admins[0]) {
                                const adminUser = admins[0];
                                switch(adminUser.language) {
                                    case language.EN: 
                                    default: {
                                        payload.notification.title = 'Alert Cancelled';
                                        payload.notification.body = 'Alert: '+alertId+' has been Cancelled by the user: '+user.username+' ('+user.email+')';
                                        result = admin.messaging().sendToDevice(tokens, payload);
                                    } break;
        
                                    case language.ES: {
                                        payload.notification.title = 'Alerta Completada';
                                        payload.notification.body = 'Alerta: '+alertId+' fue Completada por el usuario: '+user.username+' ('+user.email+')';
                                        result = admin.messaging().sendToDevice(tokens, payload);
                                    } break;
                                }
                            }
                        }
                    });
                });
            }
        }
    }
    else return null;
    return result;
});