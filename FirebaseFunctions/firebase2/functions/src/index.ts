import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

enum appointment_state {
    pending,
    accepted,
    rejected,
}

enum order_state {
    pending,
    accepted,
    cancelled,
    closed,
    inProgress,
}

enum language {
    ES,
    EN
}

admin.initializeApp(functions.config().firebase);

export const createAppointmentsDataBase = functions.https.onCall((data, context) => {
    return admin.firestore().collection('appointments').doc(data.dbName).set({}).then (() => {
        return admin.firestore().collection(data.dbRoute).doc('adminPosts').set({
            maxPostNumber: 8,
            postCount: 0,
            postNumber: 0,
        })
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('appConfiguration').set({
            dayRangeLimit: 10,
            maxUserRequestNumber: 2,
            maxUserRating: 10,
        });
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userData').set({});
    }).then(() => {
        return admin.firestore().collection(data.dbRoute).doc('userRequests').set({});
    });
});

//APP DATA----------------------------------------------------------
export const updateAppData = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute).doc('appData').set(data.appData);
});

//NOTIFICATIONS-------------------------------------------------------------
export const updateToken = functions.https.onCall((data, context) => {
    return admin.firestore().collection(data.dbRoute + '/userData/users').doc(data.userId).update({token: data.token});
});

export const onAppointmentWrite = functions.firestore.document('appointments/{dbName}/app/userRequests/requests/{requestId}')
.onWrite((request, context) => {
    
    const dbName = context.params.dbName;
    //const topic = dbName+"-admin";
    const userRequestBefore = request.before.exists ? request.before.data() : null;
    const userRequestAfter = request.after.exists ? request.after.data() : null;
    let result = null;

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: '',
            body: '',
        },
    };

    if(!request.before.exists) {
        //On Create
        const user = userRequestAfter ? userRequestAfter.user : {username: 'Guest', admin: false, language: language.EN};


        if(!user.admin) {
            //Get Admin List
            const admins: any[] = [];
            const adminTokens: string[] = [];
            return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
            .where("admin", "==", true).get().then(adminUsers => {
                adminUsers.docs.forEach(adminUser => {
                    if(adminUser.data()) {
                        admins.push(adminUser.data());
                        const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                        adminTokens.push(validToken);
                    }
                });

                //Send to Admin
                if(admins[0]) {
                    const adminUser = admins[0];
                    switch(adminUser.language) {
                        case language.EN: {
                            payload.notification.title = 'Appointment request pending.';
                            payload.notification.body = 'Created by ' + user.username;
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Solicitud de cita pendiente.';
                            payload.notification.body = 'Creada por ' + user.username;
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;
                    }
                }
            });
        }
    }
    else if(!request.after.exists) {
        //On Delete
        const user = userRequestBefore ? userRequestBefore.user : {username: 'Guest', admin: false, language: language.EN};
        const lastChange = userRequestBefore ? userRequestBefore.lastChange : {username: 'Guest', admin: false};
        const rsBefore = userRequestBefore ? userRequestBefore.state : appointment_state.pending;
        const rvBefore = userRequestBefore ? userRequestBefore.valid : true;

        //User Request Number Modification
        result = admin.firestore().collection('appointments/' + dbName + '/app/userData/users').doc(user.id).get().then(tempUser => {
            const tu = tempUser ? tempUser.data() : user;
            const rNumber = tu.requestNumber - 1;
            const lvl = rsBefore === appointment_state.accepted ? tu.level + 1 : tu.level;
            return admin.firestore().collection('appointments/' + dbName + '/app/userData/users').doc(tu.id).update({ requestNumber: rNumber, level: lvl });
        });

        if(!user.admin) {
            if(lastChange.admin && user.token && rvBefore) {
                //Send to User
                switch(user.language) {
                    case language.EN: {
                        payload.notification.title = 'An administrator has deleted your request.';
                        payload.notification.body = 'Your request did not meet what was required.';
                        result = admin.messaging().sendToDevice(user.token, payload);
                    } break;

                    case language.ES: {
                        payload.notification.title = 'Un administrador ha eliminado tu solicitud.';
                        payload.notification.body = 'Tu solicitud no cumplia con lo requerido.';
                        result = admin.messaging().sendToDevice(user.token, payload);
                    } break;
                }
            }
            else if(rvBefore) {
                //Get Admin List
                const admins: any[] = [];
                const adminTokens: string[] = [];
                return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
                .where("admin", "==", true).get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            adminTokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(rsBefore === appointment_state.accepted) {
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: {
                                    payload.notification.title = 'An appointment was canceled.';
                                    payload.notification.body = 'By ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Se cancelo una cita.';
                                    payload.notification.body = 'Cita de ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }
                });
            }
        }
    }
    else {
        //On Update
        const user = userRequestAfter ? userRequestAfter.user : {username: 'Guest', admin: false, language: language.EN};
        const lastChange = userRequestAfter ? userRequestAfter.lastChange : {username: 'Guest', admin: false};
        const rsBefore = userRequestBefore ? userRequestBefore.state : appointment_state.pending;
        const rsAfter = userRequestAfter ? userRequestAfter.state : appointment_state.pending;

        if(!user.admin) {
            if(lastChange.admin && user.token) {
                //Send to User
                if(rsBefore !== rsAfter && rsAfter === appointment_state.accepted) { //ACCEPT REQUEST
                    switch(user.language) {
                        case language.EN: {
                            payload.notification.title = 'Your request has been accepted.';
                            payload.notification.body = 'Open it for details.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Se ha aceptado tu solicitud.';
                            payload.notification.body = 'Revisa tu solicitud para los detalles.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
                else if(rsBefore !== rsAfter && rsAfter === appointment_state.rejected) //REJECT REQUEST
                {
                    switch(user.language) {
                        case language.EN: {
                            payload.notification.title = 'Your request has been rejected.';
                            payload.notification.body = 'Open it for details.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Tu solicitud ha sido rechazada.';
                            payload.notification.body = 'Revisa tu solicitud para los detalles.';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
            }
            else {
                //Get Admin List
                const admins: any[] = [];
                const adminTokens: string[] = [];
                return admin.firestore().collection('appointments/' + dbName + '/app/userData/users')
                .where("admin", "==", true).get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            adminTokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(rsBefore !== appointment_state.pending && rsAfter === appointment_state.pending) { //FROM ACCEPTED/REJECTED TO PENDING BY USER
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: {
                                    payload.notification.title = 'A user modified an appointment.';
                                    payload.notification.body = 'By ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Un usuario modifico una cita.';
                                    payload.notification.body = 'Cita de ' + user.username;
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }
                });
            }
        }
    }
    return result;
});

export const onAppointmentSaleWrite = functions.firestore.document('appointments/{dbName}/app/adminPosts/posts/{postId}')
.onCreate((post, context) => {
    const topic = context.params.dbName+"-sales";
    const adminPost = post.data();

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: adminPost.title,
            body: adminPost.content,
        },
    };

   return admin.messaging().sendToTopic(topic, payload);
});

//Apple Test
export const appleTest = functions.https.onRequest((request, response) => {

    const payload = {
        notification: {
            title: 'Salu2',
            body: 'Si sirve',
        },
    };

    response.send( 
        admin.firestore().collection('appleTest').doc('iphone').get().then(iphone => {
            const ip = iphone.data() ? iphone.data() as any : {token: ''};
            return admin.messaging().sendToDevice(
                ip.token, 
                payload
            );
        })
    );
});

export const onImageUpload = functions.storage.object().onFinalize((object, context) => {
    const imgFullName = object.name ? object.name : "image";
    const contentType = object.contentType;
    
    const branch = object.metadata ? object.metadata.branch : "none";  //ESTOS DATOS SON NECESARIOS PARA PODER USAR SUBIDA DE IMAGENES EN CUALQUIER LUGAR
    const dbName = object.metadata ? object.metadata.dbName : "none"; 
    const document = object.metadata ? object.metadata.document : "none"; 
    const collection = object.metadata ? object.metadata.collection : "none"; 
    const objectId = object.metadata ? object.metadata.objectId : "none"; 
    const imageName = imgFullName.split('/')[imgFullName.split('/').length-1];
    const imageWidth = object.metadata ? parseInt(object.metadata.width) : 9999;
    const imageHeight = object.metadata ? parseInt(object.metadata.height) : 9999;

    if(((imageWidth < 1024 && imageHeight < 1024) || (imageName.includes('_1024x1024')) || contentType === "image/gif") 
    && branch !== 'none'
    && dbName !== 'none'
    && document !== 'none'
    && collection !== 'none'
    && objectId !== 'none'
    && imageName !== 'none') {
        const dbRoute = branch + '/' + dbName + '/app/' + document + '/' + collection;

        return admin.firestore().collection(dbRoute).doc(objectId).get().then(o => {
            const obj = o.data() ? o.data() as any : {};
            const images = obj.images as string[];

            let downloadUrl = object.mediaLink ? object.mediaLink.split("?")[0] : "none"; // 'https://www.googleapis.com/download/storage/v1/b/abcbucket.appspot.com/o/songs%2Fsong1.mp3.mp3'
            const secondPartUrl = object.mediaLink ? object.mediaLink.split("?")[1] : "none"; // 'generation=123445678912345&alt=media'

            if(downloadUrl !== "none") {
                downloadUrl = downloadUrl.replace("https://www.googleapis.com/download/storage", "https://firebasestorage.googleapis.com");
                downloadUrl = downloadUrl.replace("v1", "v0");

                downloadUrl += "?" + secondPartUrl.split("&")[1]; // 'alt=media'
                const token = object.metadata ? object.metadata.firebaseStorageDownloadTokens : "none"; 
                downloadUrl += "&token=" + token; 
                
                // Update Images
                images.push(downloadUrl); 
                return admin.firestore().collection(dbRoute).doc(objectId).update({ images: images }); 
            }
            else return null; 
        });
    }
    else return null; 
});

export const onOrderWrite = functions.firestore.document('orders/{dbName}/app/userOrders/orders/{orderId}')
.onWrite((order, context) => {
    
    const dbName = context.params.dbName;
    const orderId = context.params.orderId;
    const userOrderBefore = order.before.exists ? order.before.data() : null;
    const userOrderAfter = order.after.exists ? order.after.data() : null;
    let result = null;

    const payload = { //ESTRUCTURA DE NOTIFICACION
        notification: {
            title: '',
            body: '',
        },
    };

    if(!order.before.exists) {
        //On Create
        const user = userOrderAfter ? userOrderAfter.user : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};
        const roleAdmin = (user.roles as string[]).find(role => role === "admin") ? true : false;

        if(!roleAdmin) {
            //Get Admin List
            const admins: any[] = [];
            const adminTokens: string[] = [];
            return admin.firestore().collection('orders/' + dbName + '/app/userData/users')
            .where("roles", "array-contains", "admin").get().then(adminUsers => {
                adminUsers.docs.forEach(adminUser => {
                    if(adminUser.data()) {
                        admins.push(adminUser.data());
                        const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                        adminTokens.push(validToken);
                    }
                });

                //Send to Admin
                if(admins[0]) {
                    const adminUser = admins[0];
                    switch(adminUser.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Order Pending';
                            payload.notification.body = 'Order: '+ orderId +' created by the user: '+user.username+' ('+user.email+')';
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Pedido Pendiente';
                            payload.notification.body = 'Pedido: '+ orderId +' creado por el usuario: ' +user.username+' ('+user.email+')';
                            result = admin.messaging().sendToDevice(adminTokens, payload);
                        } break;
                    }
                }
            });
        }
    }
    else if(order.before.exists && order.after.exists){ //ON UPDATE -------------------------------------------------------------
        
        // User that created the order and the last account that changed it
        const user = userOrderAfter ? userOrderAfter.user : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};
        const delivery = userOrderAfter ? userOrderAfter.delivery : {username: 'Guest', roles: ["delivery"], language: language.EN, token: "TOKEN"};
        const lastChange = userOrderAfter ? userOrderAfter.lastChange : {username: 'Guest', roles: ["user"], language: language.EN, token: "TOKEN"};
        const order = userOrderAfter ? userOrderAfter : {number: 0};

        // Order States Before and After Update
        const osBefore = userOrderBefore ? userOrderBefore.state : order_state.pending;
        const osAfter = userOrderAfter ? userOrderAfter.state : order_state.pending;

        // Roles from User
        let roleAdmin = (user.roles as string[]).find(role => role === "admin") ? true : false;

        // Roles from Last Change Account
        let lcRoleAdmin = (lastChange.roles as string[]).find(role => role === "admin") ? true : false;
        let lcRoleDelivery= (lastChange.roles as string[]).find(role => role === "delivery") && !lcRoleAdmin ? true : false;

        if(!roleAdmin) {
            if(lcRoleAdmin && user.token !== "") {
                // Send to User
                if(osBefore !== osAfter && osAfter === order_state.accepted) { // ACCEPT REQUEST
                    console.log(user);
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Your order has been accepted';
                            payload.notification.body = 'The Order #'+ order.number +' will be sent when it is ready';
                            result = admin.messaging().sendToDevice(user.token, payload);
                            console.log(result);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Se ha aceptado tu pedido';
                            payload.notification.body = 'El Pedido #'+ order.number +' sera enviado cuando este listo';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }


                    // Send to Delivery
                    const payloadDelivery = { //ESTRUCTURA DE NOTIFICACION
                        notification: {
                            title: '',
                            body: '',
                        },
                    };
                    
                    switch(delivery.language) {
                        case language.EN: 
                        default: {
                            payloadDelivery.notification.title = 'You have been assigned an order';
                            payloadDelivery.notification.body = 'Order: ' + orderId;
                            result = admin.messaging().sendToDevice(delivery.token, payloadDelivery);
                        } break;

                        case language.ES: {
                            payloadDelivery.notification.title = 'Te han asignado un pedido';
                            payloadDelivery.notification.body = 'Pedido: ' + orderId;
                            result = admin.messaging().sendToDevice(delivery.token, payloadDelivery);
                        } break;
                    }
                }
                else if(osBefore !== osAfter && osAfter === order_state.cancelled) // CANCEL REQUEST
                {
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Your order has been cancelled';
                            payload.notification.body = 'It is possible that the order #'+ order.number +' did not have the necessary requirements. (Contact us for more details)';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Tu pedido ha sido cancelado';
                            payload.notification.body = 'Es posible que el pedido #'+ order.number +' no tuviera los requerimientos necesarios. (Contactanos para mas detalles)';
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }

                    // Send to Delivery
                    const payloadDelivery = { //ESTRUCTURA DE NOTIFICACION
                        notification: {
                            title: '',
                            body: '',
                        },
                    };
                    
                    if(osBefore === order_state.accepted || osBefore === order_state.inProgress) {
                        switch(delivery.language) {
                            case language.EN: 
                            default: {
                                payloadDelivery.notification.title = 'Order Cancelled';
                                payloadDelivery.notification.body = 'The Order: ' + orderId +' was cancelled by an Administrator.';
                                result = admin.messaging().sendToDevice(delivery.token, payloadDelivery);
                            } break;
    
                            case language.ES: {
                                payloadDelivery.notification.title = 'Pedido Cancelado';
                                payloadDelivery.notification.body = 'El Pedido: ' + orderId +' fue cancelado por un Administrador.';
                                result = admin.messaging().sendToDevice(delivery.token, payloadDelivery);
                            } break;
                        }
                    }
                }
            }
            else if((lcRoleAdmin || lcRoleDelivery) && user.token !== "") {
                //Send to User
                if(osBefore !== osAfter && osAfter === order_state.inProgress) { // ORDER IN PROGRESS
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Your order is on the way';
                            payload.notification.body = 'Order #' + order.number;
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Tu pedido esta en camino';
                            payload.notification.body = 'Pedido #' + order.number;
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
                else if(osBefore !== osAfter && osAfter === order_state.closed) // ORDER CLOSED
                {
                    switch(user.language) {
                        case language.EN: 
                        default: {
                            payload.notification.title = 'Your order has been delivered';
                            payload.notification.body = 'Order #' + order.number;
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;

                        case language.ES: {
                            payload.notification.title = 'Tu pedido ha sido entregado';
                            payload.notification.body = 'Pedido #' + order.number;
                            result = admin.messaging().sendToDevice(user.token, payload);
                        } break;
                    }
                }
            }
            else {
                //Get Admin List
                const admins: any[] = [];
                const adminTokens: string[] = [];
                return admin.firestore().collection('orders/' + dbName + '/app/userData/users')
                .where("roles", "array-contains", "admin").get().then(adminUsers => {
                    adminUsers.docs.forEach(adminUser => {
                        if(adminUser.data()) {
                            admins.push(adminUser.data());
                            const validToken = (adminUser.data().token !== "" && adminUser.data().token !== null) ? adminUser.data().token : "TOKEN";
                            adminTokens.push(validToken);
                        }
                    });

                    //Send to Admin
                    if(osBefore === order_state.pending && osAfter === order_state.pending) { //FROM PENDING TO PENDING BY USER
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: 
                                default: {
                                    payload.notification.title = 'Order Modified';
                                    payload.notification.body = 'The Order: '+orderId+' has been modified by the user: '+user.username+' ('+user.email+')';
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Pedido Modificado';
                                    payload.notification.body = 'El Pedido: '+orderId+' fue modificado por el usuario: '+user.username+' ('+user.email+')';
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }

                    if(osBefore === order_state.pending && osAfter === order_state.cancelled) { //FROM PENDING TO CANCELLED BY USER
                        if(admins[0]) {
                            const adminUser = admins[0];
                            switch(adminUser.language) {
                                case language.EN: 
                                default: {
                                    payload.notification.title = 'Order Cancelled';
                                    payload.notification.body = 'The Order: '+orderId+' has been cancelled by the user: '+user.username+' ('+user.email+')';
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
    
                                case language.ES: {
                                    payload.notification.title = 'Pedido Cancelado';
                                    payload.notification.body = 'El Pedido: '+orderId+' fue cancelado por el usuario: '+user.username+' ('+user.email+')';
                                    result = admin.messaging().sendToDevice(adminTokens, payload);
                                } break;
                            }
                        }
                    }
                });
            }
        }
    }
    else return null;
    return result;
});