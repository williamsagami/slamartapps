import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/services/data.service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  d: any;

  constructor(
    public data: DataService, 
    private ds: DomSanitizer, 
    private launchNavigator: LaunchNavigator) { 
    this.d = data.appData.data;
  }

  ngOnInit() {
    this.data.getAppData().then(cloudData => {
      this.d = this.data.appData.data;
    });
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
  }

  openRoute() {
    this.launchNavigator.isAppAvailable( this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable =>{
      var app;
      if(isAvailable){
          app =  this.launchNavigator.APP.GOOGLE_MAPS;
      }else{
          console.warn("Google Maps not available - falling back to user selection");
          app =  this.launchNavigator.APP.USER_SELECT;
      }
      this.launchNavigator.navigate("646 N Fuller Ave, Los Angeles, CA 90036, USA", {
          app: app
      });
    });
  }

}
 