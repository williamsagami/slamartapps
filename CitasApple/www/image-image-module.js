(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["image-image-module"],{

/***/ "Ebmu":
/*!******************************************************************!*\
  !*** ./src/app/pages/menu/gallery/image/image-routing.module.ts ***!
  \******************************************************************/
/*! exports provided: ImagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagePageRoutingModule", function() { return ImagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _image_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./image.page */ "ZrcC");




const routes = [
    {
        path: '',
        component: _image_page__WEBPACK_IMPORTED_MODULE_3__["ImagePage"]
    }
];
let ImagePageRoutingModule = class ImagePageRoutingModule {
};
ImagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ImagePageRoutingModule);



/***/ }),

/***/ "OAlf":
/*!**********************************************************!*\
  !*** ./src/app/pages/menu/gallery/image/image.module.ts ***!
  \**********************************************************/
/*! exports provided: ImagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagePageModule", function() { return ImagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _image_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./image-routing.module */ "Ebmu");
/* harmony import */ var _image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./image.page */ "ZrcC");







let ImagePageModule = class ImagePageModule {
};
ImagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _image_routing_module__WEBPACK_IMPORTED_MODULE_5__["ImagePageRoutingModule"]
        ],
        declarations: [_image_page__WEBPACK_IMPORTED_MODULE_6__["ImagePage"]]
    })
], ImagePageModule);



/***/ })

}]);
//# sourceMappingURL=image-image-module.js.map