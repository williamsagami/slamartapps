(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-contact-module"],{

/***/ "3t+f":
/*!**************************************************************!*\
  !*** ./src/app/pages/menu/contact/contact-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: ContactPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageRoutingModule", function() { return ContactPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contact.page */ "OPQz");




const routes = [
    {
        path: '',
        component: _contact_page__WEBPACK_IMPORTED_MODULE_3__["ContactPage"]
    }
];
let ContactPageRoutingModule = class ContactPageRoutingModule {
};
ContactPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactPageRoutingModule);



/***/ }),

/***/ "Cqs6":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/contact/contact.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>CONTACT</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <!-- TELEFONOS -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"call\" color=\"#fff\"></ion-icon>\n      <ion-title>Phones</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <!-- NUMEROS -->\n        <ion-col class=\"ion-text-center\" size=\"12\" *ngFor=\"let phoneNumber of d.phoneNumbers\">\n          <ion-button class=\"cool-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"data.phoneCall(phoneNumber.number)\">\n            <div class=\"cool-text\">\n              <ion-text>{{phoneNumber.number}}</ion-text>\n            </div>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <!-- DIRECCION -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"locate\" color=\"#fff\"></ion-icon>\n      <ion-title>Address</ion-title>\n    </ion-item>\n    <div class=\"cool-line\"></div>\n    <ion-item lines=\"none\">\n      <ion-card-content>\n        {{d.address}}\n      </ion-card-content>\n    </ion-item>\n  </ion-card>\n\n  <!-- MAPA -->\n  <ion-card  class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"map\" color=\"#fff\"></ion-icon>\n      <ion-title>Location</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col class=\"ion-text-center\" size=\"12\">\n          <ion-button class=\"cool-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"openRoute()\">\n            <!-- <ion-icon slot=\"start\" [name]=\"socialNetwork.icon\" color=\"#fff\"></ion-icon> -->\n            <div class=\"cool-text\">\n              <ion-text>Start Routing</ion-text>\n            </div>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    \n    <!-- <iframe style=\"width: 100%; height: 300px; background-color: #ffffff;\"\n      [src]=\"getMapUrl()\"\n      frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe> -->\n  </ion-card >\n  <!-- Embed code -->\n  <!-- <script type=\"text/javascript\">(new Image).src = \"//googlemapsembed.net/get?r\" + escape(document.referrer);</script>\n  <script type=\"text/javascript\" src=\"https://googlemapsembed.net/embed\"></script> -->\n  <!-- END CODE -->\n\n  <!-- REDES SOCIALES -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"list\" color=\"#fff\"></ion-icon>\n      <ion-title>Social Networks</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <!-- AQUI AGREGAR ITEM DE CORREO -->\n\n    <ion-grid>\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col class=\"ion-text-center\" size=\"12\" *ngFor=\"let socialNetwork of d.socialNetworks\">\n          <ion-button class=\"cool-button\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"data.openLink(socialNetwork.link)\">\n            <ion-icon slot=\"start\" [name]=\"socialNetwork.icon\" color=\"#fff\"></ion-icon>\n            <div class=\"cool-text\">\n              <ion-text>{{socialNetwork.text}}</ion-text>\n            </div>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n</ion-content>\n");

/***/ }),

/***/ "OPQz":
/*!****************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.page.ts ***!
  \****************************************************/
/*! exports provided: ContactPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPage", function() { return ContactPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_contact_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./contact.page.html */ "Cqs6");
/* harmony import */ var _contact_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./contact.page.scss */ "PXEl");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/data.service */ "EnSQ");
/* harmony import */ var _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/launch-navigator/ngx */ "fGQ8");







let ContactPage = class ContactPage {
    constructor(data, ds, launchNavigator) {
        this.data = data;
        this.ds = ds;
        this.launchNavigator = launchNavigator;
        this.d = data.appData.data;
    }
    ngOnInit() {
        this.data.getAppData().then(cloudData => {
            this.d = this.data.appData.data;
        });
    }
    getMapUrl() {
        return this.ds.bypassSecurityTrustResourceUrl(this.d.mapSource);
    }
    openRoute() {
        this.launchNavigator.isAppAvailable(this.launchNavigator.APP.GOOGLE_MAPS).then(isAvailable => {
            var app;
            if (isAvailable) {
                app = this.launchNavigator.APP.GOOGLE_MAPS;
            }
            else {
                console.warn("Google Maps not available - falling back to user selection");
                app = this.launchNavigator.APP.USER_SELECT;
            }
            this.launchNavigator.navigate("646 N Fuller Ave, Los Angeles, CA 90036, USA", {
                app: app
            });
        });
    }
};
ContactPage.ctorParameters = () => [
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"] },
    { type: _ionic_native_launch_navigator_ngx__WEBPACK_IMPORTED_MODULE_6__["LaunchNavigator"] }
];
ContactPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-contact',
        template: _raw_loader_contact_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_contact_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ContactPage);



/***/ }),

/***/ "PXEl":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb250YWN0LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "XRdO":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/contact/contact.module.ts ***!
  \******************************************************/
/*! exports provided: ContactPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactPageModule", function() { return ContactPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contact-routing.module */ "3t+f");
/* harmony import */ var _contact_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contact.page */ "OPQz");







let ContactPageModule = class ContactPageModule {
};
ContactPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactPageRoutingModule"]
        ],
        declarations: [_contact_page__WEBPACK_IMPORTED_MODULE_6__["ContactPage"]]
    })
], ContactPageModule);



/***/ })

}]);
//# sourceMappingURL=contact-contact-module.js.map