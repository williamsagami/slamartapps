(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-post-update-post-module"],{

/***/ "3TbU":
/*!***************************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: UpdatePostPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePostPageRoutingModule", function() { return UpdatePostPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _update_post_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-post.page */ "ye+Z");




const routes = [
    {
        path: '',
        component: _update_post_page__WEBPACK_IMPORTED_MODULE_3__["UpdatePostPage"]
    }
];
let UpdatePostPageRoutingModule = class UpdatePostPageRoutingModule {
};
UpdatePostPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdatePostPageRoutingModule);



/***/ }),

/***/ "IpQk":
/*!*******************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post.module.ts ***!
  \*******************************************************************/
/*! exports provided: UpdatePostPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePostPageModule", function() { return UpdatePostPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _update_post_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-post-routing.module */ "3TbU");
/* harmony import */ var _update_post_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-post.page */ "ye+Z");







let UpdatePostPageModule = class UpdatePostPageModule {
};
UpdatePostPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_post_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdatePostPageRoutingModule"]
        ],
        declarations: [_update_post_page__WEBPACK_IMPORTED_MODULE_6__["UpdatePostPage"]]
    })
], UpdatePostPageModule);



/***/ })

}]);
//# sourceMappingURL=update-post-update-post-module.js.map