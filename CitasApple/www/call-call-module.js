(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["call-call-module"],{

/***/ "2qFv":
/*!********************************************************!*\
  !*** ./src/app/pages/menu/call/call-routing.module.ts ***!
  \********************************************************/
/*! exports provided: CallPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallPageRoutingModule", function() { return CallPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _call_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./call.page */ "p34W");




const routes = [
    {
        path: '',
        component: _call_page__WEBPACK_IMPORTED_MODULE_3__["CallPage"]
    }
];
let CallPageRoutingModule = class CallPageRoutingModule {
};
CallPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CallPageRoutingModule);



/***/ }),

/***/ "GSJb":
/*!************************************************!*\
  !*** ./src/app/pages/menu/call/call.module.ts ***!
  \************************************************/
/*! exports provided: CallPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallPageModule", function() { return CallPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _call_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./call-routing.module */ "2qFv");
/* harmony import */ var _call_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./call.page */ "p34W");







let CallPageModule = class CallPageModule {
};
CallPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _call_routing_module__WEBPACK_IMPORTED_MODULE_5__["CallPageRoutingModule"]
        ],
        declarations: [_call_page__WEBPACK_IMPORTED_MODULE_6__["CallPage"]]
    })
], CallPageModule);



/***/ })

}]);
//# sourceMappingURL=call-call-module.js.map