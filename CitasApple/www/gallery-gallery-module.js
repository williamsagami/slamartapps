(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["gallery-gallery-module"],{

/***/ "/c91":
/*!**********************************************************!*\
  !*** ./src/app/pages/menu/gallery/image/image.page.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJpbWFnZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "34y/":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/gallery/image/image.page.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar  lines=\"none\" class=\"ion-text-center\" color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeImage()\" color=\"light\">\n        <ion-icon slot=\"icon-only\" name=\"close\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n  \n  <ion-grid style=\"height: 100%\">\n    <ion-row class=\"ion-align-items-center\" style=\"height: 100%\">\n      <ion-col size=\"12\" class=\"ion-text-center\">\n        <ion-slides #slides [options]=\"slideOpts\" loop=\"true\" zoom=\"true\" *ngIf=\"viewEntered\" (ionSlidesDidLoad)=\"slides.slideTo(imageIndex, 0)\">\n          <ion-slide *ngFor=\"let image of images\">\n            <div class=\"swiper-zoom-container\">\n              <img [src]=\"image\">\n            </div>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n    \n</ion-content> \n");

/***/ }),

/***/ "3dpm":
/*!****************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.page.ts ***!
  \****************************************************/
/*! exports provided: GalleryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPage", function() { return GalleryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_gallery_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./gallery.page.html */ "CaQ5");
/* harmony import */ var _gallery_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./gallery.page.scss */ "SsBj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/data.service */ "EnSQ");
/* harmony import */ var _image_image_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./image/image.page */ "ZrcC");







let GalleryPage = class GalleryPage {
    constructor(modalController, data) {
        this.modalController = modalController;
        this.data = data;
        this.images = [
            '../assets/images/1.jpg',
            '../assets/images/2.jpg',
            '../assets/images/3.jpg',
            '../assets/images/4.jpg',
            '../assets/images/5.jpg',
            '../assets/images/6.jpg',
            '../assets/images/7.jpg',
            '../assets/images/8.jpg',
            '../assets/images/9.jpg',
            '../assets/images/10.jpg',
            '../assets/images/11.jpg',
            '../assets/images/12.jpg',
            '../assets/images/13.jpg',
            '../assets/images/14.jpg',
            '../assets/images/15.jpg',
            '../assets/images/16.jpg',
            '../assets/images/17.jpg',
            '../assets/images/18.jpg',
            '../assets/images/19.jpg',
            '../assets/images/20.jpg',
        ];
        this.d = data.appData.data;
    }
    ngOnInit() {
    }
    openImage(i) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _image_image_page__WEBPACK_IMPORTED_MODULE_6__["ImagePage"],
                cssClass: 'modal-transparency',
                componentProps: {
                    index: i + 1,
                    images: this.images,
                }
            });
            yield modal.present();
        });
    }
};
GalleryPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] }
];
GalleryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-gallery',
        template: _raw_loader_gallery_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_gallery_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], GalleryPage);



/***/ }),

/***/ "CaQ5":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/gallery/gallery.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>GALLERY</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <!-- BOTON CONTACTO -->\n  <ion-fab vertical=\"top\" horizontal=\"end\" edge slot=\"fixed\" class=\"fab-button\">\n    <ion-fab-button>\n      <ion-icon name=\"person\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"bottom\">\n      <ion-fab-button *ngFor=\"let phoneNumber of d.phoneNumbers\" (click)=\"data.phoneCall(phoneNumber.number)\" color=\"primary\">\n        <ion-icon name=\"call\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n    <ion-fab-list side=\"start\">\n      <ion-fab-button *ngFor=\"let socialNetwork of d.socialNetworks\" (click)=\"data.openLink(socialNetwork.link)\" color=\"secondary\">\n        <ion-icon [name]=\"socialNetwork.icon\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab>\n\n  <!-- GALERIA -->\n  <div  class=\"gallery\">\n    <div class=\"image\" *ngFor=\"let image of images; index as i\" button (click)=\"openImage(i)\">\n      <img [src]=\"image\">\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "DduZ":
/*!**************************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: GalleryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPageRoutingModule", function() { return GalleryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./gallery.page */ "3dpm");




const routes = [
    {
        path: '',
        component: _gallery_page__WEBPACK_IMPORTED_MODULE_3__["GalleryPage"]
    },
    {
        path: 'image',
        loadChildren: () => __webpack_require__.e(/*! import() | image-image-module */ "image-image-module").then(__webpack_require__.bind(null, /*! ./image/image.module */ "OAlf")).then(m => m.ImagePageModule)
    }
];
let GalleryPageRoutingModule = class GalleryPageRoutingModule {
};
GalleryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GalleryPageRoutingModule);



/***/ }),

/***/ "OFJq":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.module.ts ***!
  \******************************************************/
/*! exports provided: GalleryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GalleryPageModule", function() { return GalleryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./gallery-routing.module */ "DduZ");
/* harmony import */ var _gallery_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./gallery.page */ "3dpm");







let GalleryPageModule = class GalleryPageModule {
};
GalleryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _gallery_routing_module__WEBPACK_IMPORTED_MODULE_5__["GalleryPageRoutingModule"]
        ],
        declarations: [_gallery_page__WEBPACK_IMPORTED_MODULE_6__["GalleryPage"]]
    })
], GalleryPageModule);



/***/ }),

/***/ "SsBj":
/*!******************************************************!*\
  !*** ./src/app/pages/menu/gallery/gallery.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".gallery {\n  margin: 4px;\n  column-count: 2;\n  column-gap: 4px;\n}\n\n.image-container {\n  margin: 0px;\n}\n\n.image {\n  object-fit: fill;\n  height: 100%;\n  width: 100%;\n  background: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2dhbGxlcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBQUo7O0FBR0E7RUFDSSxXQUFBO0FBQUo7O0FBR0E7RUFDSSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUFBSiIsImZpbGUiOiJnYWxsZXJ5LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuLmdhbGxlcnkge1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGNvbHVtbi1jb3VudDogMjtcbiAgICBjb2x1bW4tZ2FwOiA0cHg7XG59XG5cbi5pbWFnZS1jb250YWluZXIge1xuICAgIG1hcmdpbjogMHB4O1xufVxuXG4uaW1hZ2Uge1xuICAgIG9iamVjdC1maXQ6IGZpbGw7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn0iXX0= */");

/***/ }),

/***/ "ZrcC":
/*!********************************************************!*\
  !*** ./src/app/pages/menu/gallery/image/image.page.ts ***!
  \********************************************************/
/*! exports provided: ImagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImagePage", function() { return ImagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_image_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./image.page.html */ "34y/");
/* harmony import */ var _image_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./image.page.scss */ "/c91");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");





let ImagePage = class ImagePage {
    constructor(modalController, navParams, loadingController) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.loadingController = loadingController;
        this.viewEntered = false;
        this.images = [];
        this.slideOpts = {
            loop: true,
            zoom: true,
            passiveListeners: false,
        };
        this.imageIndex = this.navParams.get('index');
        this.images = this.navParams.get('images');
    }
    ngOnInit() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
        });
    }
    ionViewDidEnter() {
        this.viewEntered = true;
        this.loading.dismiss();
    }
    closeImage() {
        this.modalController.dismiss();
    }
};
ImagePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
ImagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-image',
        template: _raw_loader_image_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_image_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ImagePage);



/***/ })

}]);
//# sourceMappingURL=gallery-gallery-module.js.map