(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "AIhh":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/home.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>HOME</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n  \n  <!-- BOTON CONTACTO -->\n  <ion-fab vertical=\"top\" horizontal=\"end\" edge slot=\"fixed\" class=\"fab-button\">\n    <ion-fab-button>\n      <ion-icon name=\"person\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-list side=\"bottom\">\n      <ion-fab-button *ngFor=\"let numero of d.phoneNumbers\" (click)=\"data.phoneCall(numero.number)\" color=\"primary\">\n        <ion-icon name=\"call\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n    <ion-fab-list side=\"start\">\n      <ion-fab-button *ngFor=\"let red of d.socialNetworks\" (click)=\"data.openLink(red.link)\" color=\"secondary\">\n        <ion-icon [name]=\"red.icon\"></ion-icon>\n      </ion-fab-button>\n    </ion-fab-list>\n  </ion-fab>\n\n  <!-- PORTADA -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\" style=\"border-style: none;\">\n      <img src=\"../assets/images/Logo.png\" class=\"full-image\" style=\"object-fit: cover; height: 240px;\">\n  </ion-card>\n  <div class=\"cool-line\"></div>\n\n  <!-- BLOG -->\n  <ion-card class=\"cool-card sub-card\" *ngIf=\"!postsReady\">\n    <div class=\"pin\" style=\"top: -5px;\"></div>\n    <ion-item>\n      <ion-progress-bar type=\"indeterminate\" color=\"secondary\"></ion-progress-bar>\n    </ion-item>\n    <div class=\"pin\" style=\"bottom: -5px;\"></div>\n  </ion-card>\n\n  <div *ngIf=\"postsReady\">\n    <ion-card class=\"animate__animated animate__fadeInUp cool-secondary-card\">\n      <ion-slides pager=\"true\" [options]=\"slideSales\" autoplay=\"true\">\n        <ion-slide *ngFor=\"let post of posts\" style=\"height: 100%;\">\n          <div class=\"post-button\" *ngIf=\"showPostEdit()\" (click)=\"viewPost(post)\">\n            <ion-icon color=\"primary\" name=\"ellipsis-horizontal\" style=\"font-size: 24px;\"></ion-icon>\n          </div>\n          <div style=\"margin-bottom: 48px;\">\n            <ion-item>\n              <ion-text class=\"cool-secondary-title\">{{post.title}}</ion-text>\n              <ion-badge *ngIf=\"post.sale !==''\" slot=\"end\" color=\"danger\" style=\"font-size: 18px;\">{{post.sale}}</ion-badge>\n            </ion-item>\n        \n            <ion-item lines=\"none\">\n              <ion-card-content [innerHtml]=\"post.content\">\n                <!-- {{post.content}} -->\n              </ion-card-content>\n            </ion-item>\n          </div>\n        </ion-slide>\n      </ion-slides>\n      <div class=\"create-post-button\" *ngIf=\"showPostCreate()\" (click)=\"createPost()\">\n        <ion-icon color=\"primary\" name=\"add\" style=\"font-size: 32px;\"></ion-icon>\n      </div>\n    </ion-card>\n\n    <div *ngIf=\"showPostCreate() && posts.length === 0\">\n      <ion-grid class=\"grid\">\n        <ion-row class=\"ion-align-items-center\">\n          <ion-col size=\"12\" class=\"ion-text-center\">\n            <ion-button class=\"cool-button\" shape=\"round\" color=\"secondary\" (click)=\"createPost()\">\n              Create Post\n              <ion-icon slot=\"end\" name=\"share\" color=\"primary\"></ion-icon>\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n\n  <!-- DESCRIPCION -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"book\" color=\"#fff\"></ion-icon>\n      <ion-title>{{d.appTitle}}</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-item lines=\"none\">\n      <ion-card-content [innerHtml]=\"d.homeDescription\">\n        {{d.homeDescription}}\n      </ion-card-content>\n    </ion-item>\n  </ion-card>\n\n  <!-- SLIDER -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-slides pager=\"true\" [options]=\"slideOpts\" autoplay=\"true\" loop=\"true\" class=\"slider\">\n      <ion-slide *ngFor=\"let image of slider\">\n        <img [src]=\"image\" style=\"width: 100%; height: 100%; object-fit: fill;\">\n      </ion-slide>\n    </ion-slides>\n  </ion-card>\n\n  <!-- SERVICIOS -->\n  <!-- <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <img src=\"../assets/images/servicios.jfif\" class=\"full-image\">\n\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"list\" color=\"#fff\"></ion-icon>\n      <ion-title>Services</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-item lines=\"none\" *ngFor=\"let service of d.homeServices\">\n      <ion-icon slot=\"start\" name=\"radio-button-on\" color=\"#fff\" style=\"font-size: 16px;\"></ion-icon>\n      <ion-text>\n        {{service}}\n      </ion-text>\n    </ion-item>\n  </ion-card> -->\n\n  <!-- HORARIO -->\n  <ion-card class=\"cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"calendar\" color=\"#fff\"></ion-icon>\n      <ion-title>Schedule</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-grid>\n      <ion-row *ngFor=\"let schedule of schedules index as i\">\n        <ion-col>\n          <div class=\"ion-text-start cool-text\">\n            <ion-text>{{schedule.day}}</ion-text>\n          </div>\n        </ion-col>\n        <ion-col>\n          <div class=\"ion-text-end cool-text\">\n            <ion-text>{{schedule.hour}}</ion-text>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-card>\n\n  <!-- VIDEO -->\n  <!-- <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <video style=\"width: 100%; background-color: #1a1a1a;\" controls>\n      <source src=\"../assets/videos/1.mp4#t=1\" type=\"video/mp4\">\n      Your browser does not support HTML5 video.\n    </video>\n  </ion-card> -->\n\n  <!-- FLYER -->\n  <!-- <ion-card class=\"cool-card\">\n    <img src=\"../assets/images/f.jfif\">\n  </ion-card> -->\n\n</ion-content>");

/***/ }),

/***/ "FkjJ":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/home/update-post/update-post.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- HEADER -->\n<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeView()\">\n        <ion-icon slot=\"icon-only\" name='close' color='light' style=\"font-size: 25px;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>POST</ion-title>\n  </ion-toolbar>\n  <div class=\"cool-line\"></div>\n</ion-header>\n\n<ion-content class=\"cool-content\">\n\n  <ion-item lines=\"none\" color=\"primary\">\n    <ion-title class=\"cool-title\">Edit Post</ion-title>\n    <ion-icon slot=\"end\" name=\"document\" color=\"secondary\"></ion-icon>\n  </ion-item>\n\n  <div class=\"cool-line\"></div>\n\n  <ion-card class=\"cool-card sub-card\" style=\"padding: 32px 0 32px 0; margin-bottom: 32px; margin-top: 32px;\">\n    <div class=\"pin\" style=\"top: -5px;\"></div>\n\n    <!-- TITULO -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>Post Title</ion-label>\n      <ion-icon slot=\"end\" name=\"create\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-item lines=\"none\">\n      <ion-input placeholder=\"Write Title\" type=\"text\" [(ngModel)]=\"title\"></ion-input>\n    </ion-item>\n  \n    <div class=\"cool-line\"></div>\n\n    <!-- OFERTA -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>{{'Sale Percentage (Optional)'}}</ion-label>\n      <ion-icon slot=\"end\" name=\"create\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-item lines=\"none\">\n      <ion-input placeholder=\"Offer Percentage from 1 to 100\" maxlength=\"3\" type=\"text\" [(ngModel)]=\"sale\"></ion-input>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n  \n    <!-- CONTENIDO -->\n    <ion-item lines=\"none\" color=\"primary\">\n      <ion-label>Post Content</ion-label>\n      <ion-icon slot=\"end\" name=\"chatbox\" color=\"secondary\"></ion-icon>\n    </ion-item>\n  \n    <ion-card class=\"cool-card\" style=\"border-radius: 8px;\">\n      <ion-textarea \n      placeholder=\"Write Content\"\n      class=\"cool-text\" \n      style=\"white-space: pre-wrap;\"\n      [(ngModel)]=\"content\"\n      style=\"height: auto;\"></ion-textarea>\n    </ion-card>\n\n    <div class=\"pin\" style=\"bottom: -5px;\"></div>\n  </ion-card>\n\n  <div class=\"cool-line\"></div>\n\n\n  <ion-grid class=\"grid\" *ngIf=\"create\">\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"12\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"createPost()\">\n          Post\n          <ion-icon slot=\"end\" name=\"share\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid\" *ngIf=\"!create\">\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"6\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"deleteConfirm()\">\n          Delete\n          <ion-icon slot=\"end\" name=\"trash\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n      \n      <ion-col size=\"6\" class=\"ion-text-center\">\n        <ion-button class=\"cool-button appointment-button\" shape=\"round\" color=\"secondary\" (click)=\"updatePost()\">\n          Update\n          <ion-icon slot=\"end\" name=\"save\" color=\"primary\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n");

/***/ }),

/***/ "Vgs5":
/*!**********************************************!*\
  !*** ./src/app/pages/menu/home/home.page.ts ***!
  \**********************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home.page.html */ "AIhh");
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss */ "r9AK");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/data.service */ "EnSQ");
/* harmony import */ var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/firebase.service */ "Z2Br");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./update-post/update-post.page */ "ye+Z");








let HomePage = class HomePage {
    constructor(data, firebaseService, modalController) {
        this.data = data;
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.posts = [];
        this.postsReady = false;
        this.slider = [
            '../assets/images/2.jpg',
            '../assets/images/10.jpg',
            '../assets/images/8.jpg',
            '../assets/images/14.jpg',
            '../assets/images/20.jpg',
        ];
        this.slideSales = {
            autoplay: {
                delay: 4000,
            }
        };
        this.slideOpts = {
            loop: true,
            autoplay: {
                delay: 2000,
            }
        };
        this.schedules = [
            {
                day: "Monday",
                hour: "Closed",
            },
            {
                day: "Tuesday",
                hour: "10:00 AM - 03:00 PM",
            },
            {
                day: "Wednesday",
                hour: "10:00 AM - 03:00 PM",
            },
            {
                day: "Thursday",
                hour: "10:00 AM - 03:00 PM",
            },
            {
                day: "Friday",
                hour: "10:00 AM - 03:00 PM",
            },
            {
                day: "Saturday",
                hour: "10:00 AM - 08:00 PM",
            },
            {
                day: "Sunday",
                hour: "Closed",
            },
        ];
        this.d = data.appData.data;
    }
    ngOnInit() {
        //Obtener Traduccion Dinamica
        this.data.getAppData().then(cloudTranslation => {
            this.d = this.data.appData.data;
        });
        //Obtener Posts
        if (this.postsRef)
            this.postsRef.unsubscribe();
        this.postsRef = this.firebaseService.getPostsRealTime().subscribe(posts => {
            if (posts)
                this.posts = posts;
            this.postsReady = true;
        });
        if (this.adminPostsRef)
            this.adminPostsRef.unsubscribe();
        this.adminPostsRef = this.firebaseService.getPostDataRealTime().subscribe(adminPosts => {
            this.adminPosts = adminPosts;
        });
    }
    showPostEdit() {
        if (this.firebaseService.userData)
            return this.firebaseService.userData.admin;
        else
            return false;
    }
    showPostCreate() {
        if (this.adminPosts) {
            return (this.showPostEdit() && this.adminPosts.postNumber < this.adminPosts.maxPostNumber);
        }
        else
            return false;
    }
    viewPost(post) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const va = yield this.modalController.create({
                component: _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_7__["UpdatePostPage"],
                componentProps: {
                    post: post,
                    create: false,
                    adminPosts: this.adminPosts,
                }
            });
            yield va.present();
        });
    }
    createPost() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const va = yield this.modalController.create({
                component: _update_post_update_post_page__WEBPACK_IMPORTED_MODULE_7__["UpdatePostPage"],
                componentProps: {
                    post: null,
                    create: true,
                    adminPosts: this.adminPosts,
                }
            });
            yield va.present();
        });
    }
};
HomePage.ctorParameters = () => [
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"] },
    { type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HomePage);



/***/ }),

/***/ "XLRv":
/*!************************************************!*\
  !*** ./src/app/pages/menu/home/home.module.ts ***!
  \************************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "wGCQ");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "Vgs5");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "ddpC":
/*!*******************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1cGRhdGUtcG9zdC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "r9AK":
/*!************************************************!*\
  !*** ./src/app/pages/menu/home/home.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".post-button {\n  position: absolute;\n  left: 32px;\n  bottom: 24px;\n  z-index: 10;\n  height: 24px;\n  font-size: medium;\n}\n\n.create-post-button {\n  position: absolute;\n  right: 24px;\n  bottom: 24px;\n  z-index: 10;\n  height: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNKIiwiZmlsZSI6ImhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBvc3QtYnV0dG9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICAgIGxlZnQ6IDMycHg7IFxuICAgIGJvdHRvbTogMjRweDsgXG4gICAgei1pbmRleDogMTA7IFxuICAgIGhlaWdodDogMjRweDtcbiAgICBmb250LXNpemU6IG1lZGl1bTtcbn1cblxuLmNyZWF0ZS1wb3N0LWJ1dHRvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlOyBcbiAgICByaWdodDogMjRweDsgXG4gICAgYm90dG9tOiAyNHB4OyBcbiAgICB6LWluZGV4OiAxMDsgXG4gICAgaGVpZ2h0OiAyNHB4O1xufSJdfQ== */");

/***/ }),

/***/ "wGCQ":
/*!********************************************************!*\
  !*** ./src/app/pages/menu/home/home-routing.module.ts ***!
  \********************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "Vgs5");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    },
    {
        path: 'update-post',
        loadChildren: () => __webpack_require__.e(/*! import() | update-post-update-post-module */ "update-post-update-post-module").then(__webpack_require__.bind(null, /*! ./update-post/update-post.module */ "IpQk")).then(m => m.UpdatePostPageModule)
    },
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "ye+Z":
/*!*****************************************************************!*\
  !*** ./src/app/pages/menu/home/update-post/update-post.page.ts ***!
  \*****************************************************************/
/*! exports provided: UpdatePostPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdatePostPage", function() { return UpdatePostPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_update_post_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./update-post.page.html */ "FkjJ");
/* harmony import */ var _update_post_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./update-post.page.scss */ "ddpC");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/firebase.service */ "Z2Br");






let UpdatePostPage = class UpdatePostPage {
    constructor(firebaseService, modalController, navParams, toastController, alertController, loadingController) {
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.navParams = navParams;
        this.toastController = toastController;
        this.alertController = alertController;
        this.loadingController = loadingController;
        this.create = false;
        this.title = "";
        this.sale = "";
        this.content = "";
        this.post = navParams.get('post');
        this.create = navParams.get('create');
        this.adminPosts = navParams.get('adminPosts');
        //console.log(this.post);
    }
    ngOnInit() {
        if (!this.create) {
            this.title = this.post.title;
            this.sale = this.post.sale;
            this.content = this.post.content;
        }
    }
    closeView() {
        this.modalController.dismiss();
    }
    showInfo(info) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: info,
                duration: 6000,
                color: 'secondary',
            });
            toast.present();
        });
    }
    allowPost() {
        return (this.title !== "" && this.content !== "");
    }
    createPost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            if (this.allowPost()) {
                var post = {};
                post.id = 'Undefined';
                post.title = this.title;
                post.sale = this.sale;
                post.content = this.content;
                post.postNumber = this.adminPosts.postCount;
                this.firebaseService.createPost(post, this.adminPosts).then(close => {
                    this.loading.dismiss();
                    this.closeView();
                    this.showInfo("Post Created Successfully.");
                });
            }
            else {
                this.showInfo("You must fill all the fields.");
                this.loading.dismiss();
            }
        });
    }
    updatePost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            if (this.allowPost()) {
                this.post.title = this.title;
                this.post.sale = this.sale;
                this.post.content = this.content;
                this.firebaseService.updatePost(this.post).then(close => {
                    this.loading.dismiss();
                    this.closeView();
                    this.showInfo("Post has been Updated.");
                });
            }
            else {
                this.showInfo("You must fill all the fields.");
                this.loading.dismiss();
            }
        });
    }
    deleteConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'cool-alert',
                header: "Delete Post",
                message: "Are you sure you want to Delete this Post?",
                buttons: [
                    {
                        text: "Cancel",
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: "Delete",
                        handler: () => {
                            this.deletePost();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    deletePost() {
        this.loadingController.create({
            message: 'Loading...',
            cssClass: 'cool-loading',
        }).then(overlay => {
            this.loading = overlay;
            this.loading.present();
            this.firebaseService.deletePost(this.post, this.adminPosts).then(close => {
                this.loading.dismiss();
                this.closeView();
                this.showInfo("Post has been Deleted.");
            });
        });
    }
};
UpdatePostPage.ctorParameters = () => [
    { type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
UpdatePostPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-update-post',
        template: _raw_loader_update_post_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_update_post_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UpdatePostPage);



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map