(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-appointment-update-appointment-module"],{

/***/ "da42":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/menu/appointments/update-appointment/update-appointment-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: UpdateAppointmentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateAppointmentPageRoutingModule", function() { return UpdateAppointmentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _update_appointment_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./update-appointment.page */ "00fO");




const routes = [
    {
        path: '',
        component: _update_appointment_page__WEBPACK_IMPORTED_MODULE_3__["UpdateAppointmentPage"]
    }
];
let UpdateAppointmentPageRoutingModule = class UpdateAppointmentPageRoutingModule {
};
UpdateAppointmentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UpdateAppointmentPageRoutingModule);



/***/ }),

/***/ "hXy6":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/menu/appointments/update-appointment/update-appointment.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: UpdateAppointmentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateAppointmentPageModule", function() { return UpdateAppointmentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _update_appointment_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-appointment-routing.module */ "da42");
/* harmony import */ var _update_appointment_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-appointment.page */ "00fO");







let UpdateAppointmentPageModule = class UpdateAppointmentPageModule {
};
UpdateAppointmentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _update_appointment_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateAppointmentPageRoutingModule"]
        ],
        declarations: [_update_appointment_page__WEBPACK_IMPORTED_MODULE_6__["UpdateAppointmentPage"]]
    })
], UpdateAppointmentPageModule);



/***/ })

}]);
//# sourceMappingURL=update-appointment-update-appointment-module.js.map