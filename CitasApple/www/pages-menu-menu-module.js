(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-menu-module"],{

/***/ "+XmF":
/*!***************************************!*\
  !*** ./src/app/guards/login.guard.ts ***!
  \***************************************/
/*! exports provided: LoginGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginGuard", function() { return LoginGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let LoginGuard = class LoginGuard {
    constructor(fbAuth, router) {
        this.fbAuth = fbAuth;
        this.router = router;
    }
    canActivate(next, state) {
        return this.fbAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(auth => {
            if (auth == null) {
                return true;
            }
            else {
                this.router.navigate(['/menu/inicio']);
                return false;
            }
        }));
    }
};
LoginGuard.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
LoginGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoginGuard);



/***/ }),

/***/ "+eMj":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.module.ts ***!
  \*******************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-routing.module */ "ckNQ");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.page */ "agdb");







let MenuPageModule = class MenuPageModule {
};
MenuPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"]
        ],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
    })
], MenuPageModule);



/***/ }),

/***/ "0H+O":
/*!************************************************!*\
  !*** ./src/app/pages/menu/call/call.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".cover {\n  margin: 0;\n  height: 50%;\n  border-radius: 0px;\n  border-width: 0;\n}\n\n.cover-image {\n  width: 100%;\n  height: 100%;\n  object-fit: cover;\n}\n\n.container {\n  height: 50%;\n  background-color: var(--ion-color-primary);\n}\n\n.header {\n  background: transparent;\n}\n\n.header ion-toolbar {\n  --background-color: transparent;\n  --ion-color-base: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2NhbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksU0FBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFDQSwwQ0FBQTtBQUNKOztBQUVBO0VBQ0ksdUJBQUE7QUFDSjs7QUFBSTtFQUNJLCtCQUFBO0VBQ0Esd0NBQUE7QUFFUiIsImZpbGUiOiJjYWxsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb3ZlciB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGhlaWdodDogNTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgICBib3JkZXItd2lkdGg6IDA7XG59XG5cbi5jb3Zlci1pbWFnZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5oZWFkZXIge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGlvbi10b29sYmFyIHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZTogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICB9XG59Il19 */");

/***/ }),

/***/ "3Gdt":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: var(--ion-color-primary);\n  box-shadow: none !important;\n}\nion-toolbar ion-title {\n  padding: 50px 0 40px 0;\n  font-weight: 900;\n  color: #ffffff;\n}\n.footer {\n  --background: #703BD5;\n}\n.container-menu-image {\n  margin: 0;\n  background: transparent;\n  width: 100%;\n  box-shadow: none !important;\n}\n.menu-image {\n  background: transparent;\n  width: 100%;\n  margin-top: 20px;\n  height: 200px;\n  object-fit: cover;\n}\n.menu-container {\n  --background: linear-gradient(to bottom, #ffd42a, #f6844d, #b65064, #5d3656, #191924);\n}\n.mb-active {\n  height: 36px;\n  width: 220px;\n  --background: var(--ion-color-primary);\n}\n.mb-deactive {\n  height: 36px;\n  width: 200px;\n}\n.menu-button {\n  --border-color: white;\n  --box-shadow: none !important;\n  font-weight: 600;\n  font-size: 14px;\n  color: white;\n}\n.account-button {\n  --border-color: white;\n  --box-shadow: none !important;\n  font-weight: 600;\n  font-size: 14px;\n  color: white;\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 5%;\n}\n.h-lines {\n  position: absolute;\n  z-index: 10;\n  top: 12px;\n  left: 3%;\n  width: 94%;\n  height: 60px;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.l-lines {\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 3%;\n  width: 2px;\n  height: 100%;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-right: transparent;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.r-lines {\n  position: absolute;\n  z-index: 10;\n  right: 3%;\n  bottom: 3%;\n  width: 2px;\n  height: 100%;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-left: transparent;\n  border-top: transparent;\n  border-bottom: transparent;\n}\n.b-lines {\n  position: absolute;\n  z-index: 10;\n  left: 3%;\n  bottom: 3%;\n  width: 94%;\n  height: 2px;\n  border-width: 2px;\n  border-style: solid;\n  border-color: white;\n  border-left: transparent;\n  border-right: transparent;\n  border-top: transparent;\n}\n.tilt {\n  position: relative;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  overflow: hidden;\n  line-height: 0;\n  transform: rotate(180deg);\n}\n.tilt svg {\n  position: relative;\n  display: block;\n  width: 100%;\n  height: 60px;\n  transform: rotateY(180deg);\n}\n.tilt .shape-fill {\n  fill: #ffd42a;\n}\n.back-circle {\n  z-index: 10;\n  width: 80px;\n  height: 80px;\n  border-radius: 50%;\n  background: var(--ion-color-primary);\n  position: absolute;\n}\n.bc-m {\n  width: 40px;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL21lbnUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0NBQUE7RUFDQSwyQkFBQTtBQUNKO0FBQ0k7RUFDSSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUNSO0FBR0E7RUFBVSxxQkFBQTtBQUNWO0FBQ0E7RUFDSSxTQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsMkJBQUE7QUFFSjtBQUNBO0VBQ0ksdUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7QUFFSjtBQUNBO0VBQ0kscUZBQUE7QUFFSjtBQUVBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxzQ0FBQTtBQUNKO0FBRUE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtBQUNKO0FBRUE7RUFFSSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQUFKO0FBR0E7RUFDSSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FBQUo7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0FBQUo7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUFBSjtBQUdBO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtBQUFKO0FBR0E7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSx5QkFBQTtFQUNBLHVCQUFBO0FBQUo7QUFJQTtFQUNJLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QUFESjtBQUlBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSwwQkFBQTtBQURKO0FBSUE7RUFDSSxhQUFBO0FBREo7QUFJQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esb0NBQUE7RUFDQSxrQkFBQTtBQURKO0FBSUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQURKIiwiZmlsZSI6Im1lbnUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcblxuICAgIGlvbi10aXRsZSB7XG4gICAgICAgIHBhZGRpbmc6IDUwcHggMCA0MHB4IDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA5MDA7XG4gICAgICAgIGNvbG9yOiAjZmZmZmZmO1xuICAgIH1cbn1cblxuLmZvb3RlciB7IC0tYmFja2dyb3VuZDogIzcwM0JENTsgfSAvL3ZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTsgfVxuXG4uY29udGFpbmVyLW1lbnUtaW1hZ2Uge1xuICAgIG1hcmdpbjogMDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5tZW51LWltYWdlIHtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGhlaWdodDogMjAwcHg7XG4gICAgb2JqZWN0LWZpdDogY292ZXI7XG59XG5cbi5tZW51LWNvbnRhaW5lciB7XG4gICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQodG8gYm90dG9tLCAjZmZkNDJhLCAjZjY4NDRkLCAjYjY1MDY0LCAjNWQzNjU2LCAjMTkxOTI0KTtcbn1cblxuLy9NZW51IEJ1dHRvbnNcbi5tYi1hY3RpdmUge1xuICAgIGhlaWdodDogMzZweDtcbiAgICB3aWR0aDogMjIwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi5tYi1kZWFjdGl2ZSB7XG4gICAgaGVpZ2h0OiAzNnB4O1xuICAgIHdpZHRoOiAyMDBweDtcbn1cblxuLm1lbnUtYnV0dG9uIHtcbiAgICAvLy0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIC0tYm9yZGVyLWNvbG9yOiB3aGl0ZTtcbiAgICAtLWJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogd2hpdGU7XG59XG5cbi5hY2NvdW50LWJ1dHRvbiB7XG4gICAgLS1ib3JkZXItY29sb3I6IHdoaXRlO1xuICAgIC0tYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogMTA7XG4gICAgbGVmdDogMyU7XG4gICAgYm90dG9tOiA1JTtcbn1cblxuLmgtbGluZXMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB0b3A6IDEycHg7XG4gICAgbGVmdDogMyU7XG4gICAgd2lkdGg6IDk0JTtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBib3JkZXItY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci10b3A6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1ib3R0b206IHRyYW5zcGFyZW50O1xufVxuXG4ubC1saW5lcyB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIGxlZnQ6IDMlO1xuICAgIGJvdHRvbTogMyU7XG4gICAgd2lkdGg6IDJweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBib3JkZXItY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yaWdodDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLXRvcDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyLWJvdHRvbTogdHJhbnNwYXJlbnQ7XG59XG5cbi5yLWxpbmVzIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogMTA7XG4gICAgcmlnaHQ6IDMlO1xuICAgIGJvdHRvbTogMyU7XG4gICAgd2lkdGg6IDJweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBib3JkZXItY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1sZWZ0OiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItdG9wOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiB0cmFuc3BhcmVudDtcbn1cblxuLmItbGluZXMge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDtcbiAgICBsZWZ0OiAzJTtcbiAgICBib3R0b206IDMlO1xuICAgIHdpZHRoOiA5NCU7XG4gICAgaGVpZ2h0OiAycHg7XG4gICAgYm9yZGVyLXdpZHRoOiAycHg7XG4gICAgYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgICBib3JkZXItY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1sZWZ0OiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItcmlnaHQ6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci10b3A6IHRyYW5zcGFyZW50O1xufVxuXG4vL1NoYXBlc1xuLnRpbHQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGxpbmUtaGVpZ2h0OiAwO1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XG59XG5cbi50aWx0IHN2ZyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNjBweDtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVkoMTgwZGVnKTtcbn1cblxuLnRpbHQgLnNoYXBlLWZpbGwge1xuICAgIGZpbGw6ICNmZmQ0MmE7XG59XG5cbi5iYWNrLWNpcmNsZSB7XG4gICAgei1pbmRleDogMTA7XG4gICAgd2lkdGg6IDgwcHg7XG4gICAgaGVpZ2h0OiA4MHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xufVxuXG4uYmMtbSB7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA0MHB4O1xufSJdfQ== */");

/***/ }),

/***/ "EnSQ":
/*!******************************************!*\
  !*** ./src/app/services/data.service.ts ***!
  \******************************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");
/* harmony import */ var _pages_menu_call_call_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../pages/menu/call/call.page */ "p34W");
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./firebase.service */ "Z2Br");






let DataService = class DataService {
    constructor(firebaseService, modalController, iab) {
        // !!!!!!!!!!!! METODO PARA CAMBIO DE TRADUCCION Y DATOS
        this.firebaseService = firebaseService;
        this.modalController = modalController;
        this.iab = iab;
        //Datos de Aplicacion--------------------------------------------------------------
        //En este objeto se almacenan todos los datos que pueden cambiar por peticion del usuario para actualizaciones rapidas 
        this.appData = {
            //Datos de Contacto
            data: {
                appTitle: "Fade Luxe",
                homeDescription: `Fade Luxe Barbershop is appointment only. We are licensed professionals and we take pride in our craft. <br>
        We strive to meet your needs and present you with a clean haircut and end result. We keep things sanitary for yours and our protection. <br>
        We’ve got it under control here at Fade Luxe. Come get a haircut.`,
                homeServices: [
                    "Nails",
                    "Hair",
                    "Microblading",
                    "Manicure and Pedicure",
                    "Eyelashes", "Hair Extensions",
                    "Facials and Much More",
                ],
                phoneNumbers: [
                    {
                        text: "Phone",
                        number: "323 433 4400",
                    }
                ],
                socialNetworks: [
                    /*{
                      text: "Madelin's Salon",
                      link: "https://www.facebook.com/Madelins-Salon-108182611103315/",
                      icon: "logo-facebook",
                    },*/
                    {
                        text: "fadeluxebarbershop",
                        link: "https://www.instagram.com/fadeluxebarbershop/",
                        icon: "logo-instagram",
                    }
                ],
                address: "646 N Fuller Ave, Los Angeles, CA 90036, USA",
                mapSource: "https://maps.google.com/maps?q=fade%20lux%20barbershop&t=&z=15&ie=UTF8&iwloc=&output=embed",
            }
        };
        //this.firebaseService.updateAppData(this.appData).then(t => {console.log(t)});
    }
    phoneCall(phoneNumber) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _pages_menu_call_call_page__WEBPACK_IMPORTED_MODULE_4__["CallPage"],
                componentProps: {
                    number: phoneNumber,
                }
            });
            yield modal.present();
        });
    }
    openLink(link) {
        this.iab.create(link);
    }
    getAppData() {
        return this.firebaseService.getAppData().then(appData => {
            if (appData) {
                this.appData = appData;
                //console.log(appData);
            }
        });
    }
};
DataService.ctorParameters = () => [
    { type: _firebase_service__WEBPACK_IMPORTED_MODULE_5__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__["InAppBrowser"] }
];
DataService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ "FJ4N":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/call/call.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-button (click)=\"closeCall()\">\n        <ion-icon slot=\"icon-only\" name='close' color='light' style=\"font-size: 25px;\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"cover\">\n    <img src=\"../assets/images/call.jpg\" class=\"cover-image\">\n  </div>\n\n  <ion-grid class=\"container\">\n    <ion-row style=\"height: 100%;\">\n      <ion-col class=\"ion-text-center\" size=\"12\">\n        \n        <ion-card class=\"cool-card\" style=\"border-width: 0; box-shadow: none;\">\n          <ion-item lines=\"none\">\n            <ion-title>Call</ion-title>\n          </ion-item>\n        </ion-card>\n\n        <div class=\"cool-line\" style=\"margin-bottom: 16px;\"></div>\n\n        <ion-button class=\"cool-button\" style=\"height: 120px;\" shape=\"round\" fill=\"outline\" lines=\"none\" (click)=\"phoneCall(phoneNumber)\">\n          <ion-icon slot=\"start\" name=\"call\" color=\"#fff\"></ion-icon>\n          <div class=\"cool-text\">\n            <ion-text>{{phoneNumber}}</ion-text>\n          </div>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "UTcu":
/*!**************************************!*\
  !*** ./src/app/guards/auth.guard.ts ***!
  \**************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let AuthGuard = class AuthGuard {
    constructor(fbAuth, router) {
        this.fbAuth = fbAuth;
        this.router = router;
    }
    canActivate(next, state) {
        return this.fbAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(auth => {
            if (auth == null) {
                this.router.navigate(['/menu/login']);
                return false;
            }
            else
                return true;
        }));
    }
};
AuthGuard.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AuthGuard);



/***/ }),

/***/ "V3Oe":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/menu/menu.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-menu contentId=\"sidemenu\" type=\"overlay\">\n\n  <ion-header class=\"ion-no-border\">\n    <ion-toolbar>\n      <ion-card class=\"animate__bounceIn container-menu-image\">\n        <img class=\"menu-image\" src=\"../assets/images/Logo.png\">\n      </ion-card>\n      <div class=\"tilt\">\n        <svg data-name=\"Layer 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1200 120\" preserveAspectRatio=\"none\">\n          <path d=\"M1200 120L0 16.48 0 0 1200 0 1200 120z\" class=\"shape-fill\"></path>\n        </svg>\n      </div>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content class=\"menu-container\"> \n    <ion-grid style=\"padding-top: 8px;\">\n      <ion-row class=\"ion-align-items-center\">\n        <ion-col size=\"12\" class=\"ion-text-center\" *ngFor=\"let page of pages; index as i\">\n          <ion-menu-toggle auto-hide=\"false\">\n            <ion-button class=\"animate__animated menu-button\" shape=\"round\" fill=\"outline\" lines=\"none\"\n              [routerLink]=\"page.url\" \n              routerDirection=\"root\"\n              [class.mb-active]=\"selectedUrl === page.url\"\n              [class.mb-deactive]=\"selectedUrl != page.url\"\n              [class.animate__heartBeat]=\"selectedUrl === page.url\">\n              <ion-icon [name]=\"page.icon\" slot=\"end\" color=\"light\"></ion-icon>\n              <span class=\"ion-text-left\" style=\"margin-right: auto;\">{{page.text}}</span>\n            </ion-button>\n          </ion-menu-toggle>\n        </ion-col>\n        <ion-col size=\"12\" class=\"ion-text-center\">\n          <div *ngIf=\"showSubscriptionButton()\">\n            <ion-menu-toggle auto-hide=\"false\">\n              <ion-button class=\"animate__animated menu-button\" shape=\"round\" (click)=\"toggleSalesNotifications()\">\n                <ion-icon name=\"star\" slot=\"end\" color=\"light\"></ion-icon>\n                <span class=\"ion-text-left\" style=\"margin-right: auto;\">Join Us</span>\n              </ion-button>\n            </ion-menu-toggle>\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-menu-toggle auto-hide=\"false\">\n      <ion-button class=\"account-button\" shape=\"round\" fill=\"clear\" lines=\"none\" routerLink=\"/menu/account\">Account</ion-button>\n    </ion-menu-toggle>\n\n    <div class=\"l-lines\"></div>\n    <div class=\"r-lines\"></div>\n    <div class=\"b-lines\"></div>\n\n  </ion-content>\n\n</ion-menu>\n\n<ion-router-outlet id=\"sidemenu\"></ion-router-outlet>\n");

/***/ }),

/***/ "Z2Br":
/*!**********************************************!*\
  !*** ./src/app/services/firebase.service.ts ***!
  \**********************************************/
/*! exports provided: state, language, FirebaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "state", function() { return state; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "language", function() { return language; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirebaseService", function() { return FirebaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/auth */ "UbJi");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/firestore */ "I/3d");
/* harmony import */ var _angular_fire_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/functions */ "RgrY");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/firebase-x/ngx */ "E9qw");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");








var state;
(function (state) {
    state[state["pending"] = 0] = "pending";
    state[state["accepted"] = 1] = "accepted";
    state[state["rejected"] = 2] = "rejected";
})(state || (state = {}));
var language;
(function (language) {
    language[language["ES"] = 0] = "ES";
    language[language["EN"] = 1] = "EN";
})(language || (language = {}));
let FirebaseService = class FirebaseService {
    constructor(fbAuth, db, functions, fcm, platform) {
        this.fbAuth = fbAuth;
        this.db = db;
        this.functions = functions;
        this.fcm = fcm;
        this.platform = platform;
        this.dbName = "fadeluxe";
        this.dbRoute = "appointments/fadeluxe/app";
        this.userReady = false;
        this.defaultLanguage = language.EN;
        this.getAppData().then(appData => {
            this.appData = appData;
        });
    }
    getDataBaseName() { return this.dbName; }
    //SETUP APP --------------------------------------------------------------------
    userSetup() {
        return new Promise((resolve, rejected) => {
            if (this.userReady) {
                var userDataExist = this.userData ? true : false;
                resolve(userDataExist);
            }
            else {
                if (this.authRef)
                    this.authRef.unsubscribe(); //Limpia las referencias de suscripcion
                if (this.userRef)
                    this.userRef.unsubscribe();
                this.authRef = this.fbAuth.authState.subscribe(user => {
                    this.userAuthState = user;
                    //console.log(user);
                    if (user) {
                        this.userRef = this.getUserRealTime(user.uid).subscribe(userData => {
                            this.userData = userData;
                            //console.log(userData);
                            //Si existe el usuario devuelve el true
                            if (userData) {
                                this.userReady = true;
                                resolve(true);
                            }
                            //Si no existe se limpia la referencia de suscripcion y se crea un usuario nuevo, para despues tomar los datos en real time
                            else {
                                this.userRef.unsubscribe();
                                this.createUser(user.uid, user.email).then(createdUser => {
                                    this.userData = createdUser;
                                    this.userRef = this.getUserRealTime(user.uid).subscribe(userData => {
                                        this.userData = userData;
                                        console.log(userData);
                                        this.userReady = true;
                                        resolve(true);
                                    });
                                });
                            }
                        });
                    }
                    //Si no esta logeado
                    else {
                        this.userReady = true;
                        this.userData = null;
                        resolve(false);
                    }
                });
            }
        });
    }
    isUserReady() { return this.userReady; }
    //AUTENTICACION----------------------------------------------------------
    login(email, password) {
        return new Promise((resolve, rejected) => {
            this.fbAuth.signInWithEmailAndPassword(email, password).then(res => {
                this.user = res.user;
                this.userReady = false;
                resolve(res);
            }).catch(err => rejected(err));
        }).then(setup => {
            this.userSetup().then(saveToken => {
                console.log(this.user.uid);
                this.saveToken(this.user.uid);
            });
        });
    }
    register(email, password) {
        return new Promise((resolve, reject) => {
            this.fbAuth.createUserWithEmailAndPassword(email, password).then(res => {
                this.user = res.user;
                resolve(res);
            }).catch(err => reject(err));
        }).then(setup => {
            this.createUser(this.user.uid, email).then(userSetup => {
                this.userReady = false;
                this.userSetup();
            });
        });
    }
    logout() {
        this.userReady = false;
        this.userAuthState = null;
        this.user = null;
        return this.fbAuth.signOut();
    }
    //GUARDAR TOKENS-------------------------------------------------------
    updateToken(userId, token) {
        return this.db.collection(this.dbRoute + '/userData/users').doc(userId).update({ token: token });
    }
    saveToken(userId) {
        if (this.platform.is('android')) {
            return this.fcm.getToken().then(token => {
                this.updateToken(userId, token);
            });
        }
        else if (this.platform.is('ios')) {
            return this.fcm.getToken().then(token => {
                this.updateToken(userId, token).then(permission => {
                    this.fcm.grantPermission();
                });
            });
        }
        else
            return null;
    }
    //TRADUCCION----------------------------------------------------------
    updateAppData(appData) {
        const functionRef = this.functions.httpsCallable('updateAppData');
        return functionRef({ dbRoute: this.dbRoute, appData: appData }).toPromise();
    }
    getAppData() {
        return new Promise((resolve, reject) => {
            if (this.appData) {
                resolve(this.appData);
            }
            else
                this.db.collection(this.dbRoute).doc('appData').ref.get().then(res => {
                    resolve(res.data());
                }).catch(err => reject(err));
        });
    }
    //OPCIONES DE APLICACION-----------------------------------------
    updateAppConfiguration(appConfig) {
        return this.db.collection(this.dbRoute).doc('appConfiguration').update({
            dayRangeLimit: appConfig.dayRangeLimit,
            maxUserRequestNumber: appConfig.maxUserRequestNumber,
        });
    }
    getAppConfiguration() {
        return new Promise((resolve, reject) => {
            this.db.collection(this.dbRoute).doc('appConfiguration').ref.get().then(res => {
                resolve(res.data());
            }).catch(err => reject(err));
        });
    }
    //BLOG----------------------------------------------------------
    getPostDataRealTime() {
        return this.db.collection(this.dbRoute).doc('adminPosts').snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(postData => {
            return postData.payload.data();
        }));
    }
    getPostsRealTime() {
        return this.db.collection(this.dbRoute + '/adminPosts/posts', ref => ref.orderBy('postNumber', 'desc')).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(posts => {
            return posts.map(post => {
                const postData = post.payload.doc.data();
                postData.id = post.payload.doc.id;
                return postData;
            });
        }));
    }
    createPost(post, postsData) {
        return this.db.collection(this.dbRoute + '/adminPosts/posts').doc('p_' + post.postNumber).set(post).then(postNumber => {
            var pNumber = postsData.postNumber + 1;
            var pCount = postsData.postCount + 1;
            this.db.collection(this.dbRoute).doc('adminPosts').update({ postNumber: pNumber, postCount: pCount });
        });
    }
    updatePost(post) {
        return this.db.collection(this.dbRoute + '/adminPosts/posts').doc(post.id).update(post);
    }
    deletePost(post, postsData) {
        return this.db.collection(this.dbRoute + '/adminPosts/posts').doc(post.id).delete().then(postNumber => {
            const pNumber = postsData.postNumber - 1;
            this.db.collection(this.dbRoute).doc('adminPosts').update({ postNumber: pNumber });
        });
    }
    //USUARIOS----------------------------------------------------------
    getUser(user_id) {
        return new Promise((resolve, reject) => {
            this.db.collection(this.dbRoute + '/userData/users').doc(user_id).ref.get().then(res => {
                resolve(res.data());
            }).catch(err => reject(err));
        });
    }
    getUserRealTime(user_id) {
        return this.db.collection(this.dbRoute + '/userData/users').doc(user_id).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(user => {
            return user.payload.data();
        }));
    }
    updateUser(user) {
        return this.db.collection(this.dbRoute + '/userData/users').doc(user.id).update(user);
    }
    //CITAS----------------------------------------------------------
    getRequests() {
        return new Promise((resolve, reject) => {
            this.db.collection(this.dbRoute + '/userRequests/requests', ref => ref
                .orderBy('creationDate')).ref.get().then(res => {
                var requests = res.docs.map(request => {
                    const requestData = request.data();
                    requestData.date = requestData.date.toDate();
                    requestData.creationDate = requestData.creationDate.toDate();
                    return requestData;
                });
                resolve(requests);
            }).catch(err => reject(err));
        });
    }
    getRequestsRealTime() {
        return this.db.collection(this.dbRoute + '/userRequests/requests', ref => ref
            .orderBy('creationDate')).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(requests => {
            return requests.map(request => {
                const requestData = request.payload.doc.data();
                requestData.date = requestData.date.toDate();
                requestData.creationDate = requestData.creationDate.toDate();
                return requestData;
            });
        }));
    }
    getUserRequests(user_id) {
        return new Promise((resolve, reject) => {
            this.db.collection(this.dbRoute + '/userRequests/requests', ref => ref
                .where("user.id", "==", user_id)).ref.get().then(res => {
                var requests = res.docs.map(request => {
                    const requestData = request.data();
                    requestData.date = requestData.date.toDate();
                    requestData.creationDate = requestData.creationDate.toDate();
                    return requestData;
                });
                resolve(requests);
            }).catch(err => reject(err));
        });
    }
    getUserRequestsRealTime(user_id) {
        return this.db.collection(this.dbRoute + '/userRequests/requests', ref => ref
            .where("user.id", "==", user_id)).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(requests => {
            return requests.map(request => {
                const requestData = request.payload.doc.data();
                requestData.date = requestData.date.toDate();
                requestData.creationDate = requestData.creationDate.toDate();
                return requestData;
            });
        }));
    }
    acceptRequest(request) {
        request.state = state.accepted;
        this.updateRequest(request);
    }
    rejectRequest(request) {
        request.state = state.rejected;
        this.updateRequest(request);
    }
    updateRequest(request) {
        const arId = request.user.id + request.id;
        return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId).update(request);
    }
    createRequest(request, userData) {
        const arId = request.user.id + request.id;
        const rCount = userData.requestCount + 1;
        const rNumber = userData.requestNumber + 1;
        return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId).set(request).then(requestCount => {
            this.db.collection(this.dbRoute + '/userData/users').doc(request.user.id).update({ requestCount: rCount, requestNumber: rNumber });
        });
    }
    deleteRequest(request, validRequest) {
        const arId = request.user.id + request.id;
        return this.db.collection(this.dbRoute + '/userRequests/requests').doc(arId).delete().then(requestCount => {
            if (validRequest) {
                this.getUser(request.user.id).then(tempUser => {
                    const tu = tempUser;
                    const cancel = tu.cancel + 1;
                    if (request.state === state.accepted) {
                        return this.db.collection(this.dbRoute + '/userData/users').doc(tu.id).update({ cancel: cancel });
                    }
                });
            }
        });
    }
    createUser(user_id, email) {
        var user = {};
        user.id = user_id;
        user.username = "Guest";
        user.admin = false;
        user.phone = "Undefined";
        user.email = email;
        user.rating = 0;
        user.cancel = 0;
        user.level = 1;
        user.requestCount = 0;
        user.requestNumber = 0;
        user.allowRequests = true;
        var coolUser = user;
        coolUser.language = language.EN;
        coolUser.notifications = false;
        coolUser.token = "";
        //SE NECESITA REVISION AQUI
        return new Promise((resolve, reject) => {
            this.db.collection(this.dbRoute + '/userData/users').doc(user.id).set(coolUser).then(createdUser => {
                this.saveToken(user_id).then(created => {
                    resolve(coolUser);
                }).catch(err => {
                    resolve(coolUser);
                });
            });
        });
    }
};
FirebaseService.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_2__["AngularFireAuth"] },
    { type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"] },
    { type: _angular_fire_functions__WEBPACK_IMPORTED_MODULE_4__["AngularFireFunctions"] },
    { type: _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_6__["FirebaseX"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] }
];
FirebaseService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FirebaseService);



/***/ }),

/***/ "agdb":
/*!*****************************************!*\
  !*** ./src/app/pages/menu/menu.page.ts ***!
  \*****************************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_menu_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./menu.page.html */ "V3Oe");
/* harmony import */ var _menu_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./menu.page.scss */ "3Gdt");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/data.service */ "EnSQ");
/* harmony import */ var src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/firebase.service */ "Z2Br");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");








let MenuPage = class MenuPage {
    constructor(router, data, firebaseService, toastController) {
        this.router = router;
        this.data = data;
        this.firebaseService = firebaseService;
        this.toastController = toastController;
        this.userReady = false;
        this.selectedUrl = '/menu/inicio';
        this.pages = [
            {
                text: "Home",
                url: '/menu/home',
                icon: "home",
            },
            {
                text: "Appointments",
                url: '/menu/appointments',
                icon: "list",
            },
            {
                text: "Gallery",
                url: '/menu/gallery',
                icon: "images",
            },
            {
                text: "Contact",
                url: '/menu/contact',
                icon: "person",
            }
        ];
        this.router.events.subscribe((event) => {
            this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
        });
        this.d = data.appData.data;
    }
    ngOnInit() {
        this.firebaseService.userSetup().then(userDataExist => {
            this.userReady = userDataExist;
        });
    }
    showInfo(info) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: info,
                duration: 6000,
                color: 'secondary',
            });
            toast.present();
        });
    }
    showSubscriptionButton() {
        if (this.userReady) {
            if (this.firebaseService.userData) {
                if (!this.firebaseService.userData.admin)
                    return !this.firebaseService.userData.notifications;
                else
                    return false;
            }
        }
        else
            return false;
    }
    toggleSalesNotifications() {
        if (this.userReady) {
            if (!this.firebaseService.userData.notifications) {
                var userData = this.firebaseService.userData;
                userData.notifications = true;
                this.firebaseService.updateUser(userData).then(updateUser => {
                    this.firebaseService.fcm.subscribe(this.firebaseService.getDataBaseName() + "-sales").then(showInfo => {
                        this.showInfo("Notifications On.");
                    });
                });
            }
        }
        else {
            this.router.navigate(['/menu/login']);
        }
    }
};
MenuPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] },
    { type: src_app_services_firebase_service__WEBPACK_IMPORTED_MODULE_6__["FirebaseService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"] }
];
MenuPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-menu',
        template: _raw_loader_menu_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_menu_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MenuPage);



/***/ }),

/***/ "ckNQ":
/*!***************************************************!*\
  !*** ./src/app/pages/menu/menu-routing.module.ts ***!
  \***************************************************/
/*! exports provided: MenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function() { return MenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.page */ "agdb");
/* harmony import */ var _guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../guards/auth.guard */ "UTcu");
/* harmony import */ var _guards_login_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../guards/login.guard */ "+XmF");






const routes = [
    {
        path: 'menu',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"],
        children: [
            {
                path: 'home',
                loadChildren: () => __webpack_require__.e(/*! import() | home-home-module */ "home-home-module").then(__webpack_require__.bind(null, /*! ./home/home.module */ "XLRv")).then(m => m.HomePageModule)
            },
            {
                path: 'login',
                loadChildren: () => __webpack_require__.e(/*! import() | login-login-module */ "login-login-module").then(__webpack_require__.bind(null, /*! ./login/login.module */ "WDWl")).then(m => m.LoginPageModule),
                canActivate: [_guards_login_guard__WEBPACK_IMPORTED_MODULE_5__["LoginGuard"]],
            },
            {
                path: 'gallery',
                loadChildren: () => __webpack_require__.e(/*! import() | gallery-gallery-module */ "gallery-gallery-module").then(__webpack_require__.bind(null, /*! ./gallery/gallery.module */ "OFJq")).then(m => m.GalleryPageModule)
            },
            {
                path: 'account',
                loadChildren: () => __webpack_require__.e(/*! import() | account-account-module */ "account-account-module").then(__webpack_require__.bind(null, /*! ./account/account.module */ "XuPe")).then(m => m.AccountPageModule),
                canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
            },
            {
                path: 'contact',
                loadChildren: () => __webpack_require__.e(/*! import() | contact-contact-module */ "contact-contact-module").then(__webpack_require__.bind(null, /*! ./contact/contact.module */ "XRdO")).then(m => m.ContactPageModule)
            },
            {
                path: 'appointments',
                loadChildren: () => __webpack_require__.e(/*! import() | appointments-appointments-module */ "appointments-appointments-module").then(__webpack_require__.bind(null, /*! ./appointments/appointments.module */ "ar1t")).then(m => m.AppointmentsPageModule),
                canActivate: [_guards_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
            },
        ]
    },
    {
        path: 'call',
        loadChildren: () => __webpack_require__.e(/*! import() | call-call-module */ "call-call-module").then(__webpack_require__.bind(null, /*! ./call/call.module */ "GSJb")).then(m => m.CallPageModule)
    },
    {
        path: '',
        redirectTo: '/menu/home',
        pathMatch: 'full'
    },
];
let MenuPageRoutingModule = class MenuPageRoutingModule {
};
MenuPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MenuPageRoutingModule);



/***/ }),

/***/ "p34W":
/*!**********************************************!*\
  !*** ./src/app/pages/menu/call/call.page.ts ***!
  \**********************************************/
/*! exports provided: CallPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallPage", function() { return CallPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_call_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./call.page.html */ "FJ4N");
/* harmony import */ var _call_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./call.page.scss */ "0H+O");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "Wwn5");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");






let CallPage = class CallPage {
    constructor(modalController, navParams, callNumber) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.phoneNumber = this.navParams.get('number');
    }
    ngOnInit() {
    }
    phoneCall() {
        this.callNumber.callNumber(this.phoneNumber, false)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }
    closeCall() {
        this.modalController.dismiss();
    }
};
CallPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"] },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_4__["CallNumber"] }
];
CallPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-call',
        template: _raw_loader_call_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_call_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CallPage);



/***/ })

}]);
//# sourceMappingURL=pages-menu-menu-module.js.map