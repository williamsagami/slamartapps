import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from 'src/app/services/data.service';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(
    private ds: DomSanitizer, 
    public dataService: DataService,
    private firebaseService: FirebaseService) {
  }

  ngOnInit() {
  }

  getData() {
    return this.firebaseService.appConfiguration;
  }

  getMapUrl() {
    return this.ds.bypassSecurityTrustResourceUrl(this.getData()?.mapSource);
  }

}
 